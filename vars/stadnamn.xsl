<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    xmlns:ubbef="http://data.ub.uib.no/ns/saxon/extension-functions#"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3.0">
    <xsl:variable name="språksamlingane-uri" select="$stadnamn-prefix || ubbef:uuid-from-string('https://spraksamlingane.no')"/>
    <xsl:variable name="stadnamn-prefix" select="'http://data.toponymi.spraksamlingane.no/id/'"/>
    <xsl:variable name="autoritets-endpoint" select="'http://158.39.48.37/stedsnavn-vocab?query='"/>
    <xsl:variable name="stadnamn-graph-prefix" select="'http://data.toponym.ub.uib.no/graph/'"/>
    
</xsl:stylesheet>