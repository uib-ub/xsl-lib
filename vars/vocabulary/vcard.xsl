<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
       <xsl:variable name="_vcard_origin" select="'https://www.w3.org/2006/vcard/ns#.rdf'"/>
        
        <!--Classes-->
        <xsl:variable name="vcard_Acquaintance"
            select="'http://www.w3.org/2006/vcard/ns#Acquaintance'"/>
        <xsl:variable name="vcard_Address" select="'http://www.w3.org/2006/vcard/ns#Address'"/>
        <xsl:variable name="vcard_Agent" select="'http://www.w3.org/2006/vcard/ns#Agent'"/>
        <xsl:variable name="vcard_BBS" select="'http://www.w3.org/2006/vcard/ns#BBS'"/>
        <xsl:variable name="vcard_Car" select="'http://www.w3.org/2006/vcard/ns#Car'"/>
        <xsl:variable name="vcard_Cell" select="'http://www.w3.org/2006/vcard/ns#Cell'"/>
        <xsl:variable name="vcard_Child" select="'http://www.w3.org/2006/vcard/ns#Child'"/>
        <xsl:variable name="vcard_Colleague"
            select="'http://www.w3.org/2006/vcard/ns#Colleague'"/>
        <xsl:variable name="vcard_Contact" select="'http://www.w3.org/2006/vcard/ns#Contact'"/>
        <xsl:variable name="vcard_Coresident"
            select="'http://www.w3.org/2006/vcard/ns#Coresident'"/>
        <xsl:variable name="vcard_Coworker"
            select="'http://www.w3.org/2006/vcard/ns#Coworker'"/>
        <xsl:variable name="vcard_Crush" select="'http://www.w3.org/2006/vcard/ns#Crush'"/>
        <xsl:variable name="vcard_Date" select="'http://www.w3.org/2006/vcard/ns#Date'"/>
        <xsl:variable name="vcard_Dom" select="'http://www.w3.org/2006/vcard/ns#Dom'"/>
        <xsl:variable name="vcard_Email" select="'http://www.w3.org/2006/vcard/ns#Email'"/>
        <xsl:variable name="vcard_Emergency"
            select="'http://www.w3.org/2006/vcard/ns#Emergency'"/>
        <xsl:variable name="vcard_Fax" select="'http://www.w3.org/2006/vcard/ns#Fax'"/>
        <xsl:variable name="vcard_Female" select="'http://www.w3.org/2006/vcard/ns#Female'"/>
        <xsl:variable name="vcard_Friend" select="'http://www.w3.org/2006/vcard/ns#Friend'"/>
        <xsl:variable name="vcard_Gender" select="'http://www.w3.org/2006/vcard/ns#Gender'"/>
        <xsl:variable name="vcard_Group" select="'http://www.w3.org/2006/vcard/ns#Group'"/>
        <xsl:variable name="vcard_Home" select="'http://www.w3.org/2006/vcard/ns#Home'"/>
        <xsl:variable name="vcard_ISDN" select="'http://www.w3.org/2006/vcard/ns#ISDN'"/>
        <xsl:variable name="vcard_Individual"
            select="'http://www.w3.org/2006/vcard/ns#Individual'"/>
        <xsl:variable name="vcard_Internet"
            select="'http://www.w3.org/2006/vcard/ns#Internet'"/>
        <xsl:variable name="vcard_Intl" select="'http://www.w3.org/2006/vcard/ns#Intl'"/>
        <xsl:variable name="vcard_Kin" select="'http://www.w3.org/2006/vcard/ns#Kin'"/>
        <xsl:variable name="vcard_Kind" select="'http://www.w3.org/2006/vcard/ns#Kind'"/>
        <xsl:variable name="vcard_Label" select="'http://www.w3.org/2006/vcard/ns#Label'"/>
        <xsl:variable name="vcard_Location"
            select="'http://www.w3.org/2006/vcard/ns#Location'"/>
        <xsl:variable name="vcard_Male" select="'http://www.w3.org/2006/vcard/ns#Male'"/>
        <xsl:variable name="vcard_Me" select="'http://www.w3.org/2006/vcard/ns#Me'"/>
        <xsl:variable name="vcard_Met" select="'http://www.w3.org/2006/vcard/ns#Met'"/>
        <xsl:variable name="vcard_Modem" select="'http://www.w3.org/2006/vcard/ns#Modem'"/>
        <xsl:variable name="vcard_Msg" select="'http://www.w3.org/2006/vcard/ns#Msg'"/>
        <xsl:variable name="vcard_Muse" select="'http://www.w3.org/2006/vcard/ns#Muse'"/>
        <xsl:variable name="vcard_Name" select="'http://www.w3.org/2006/vcard/ns#Name'"/>
        <xsl:variable name="vcard_Neighbor"
            select="'http://www.w3.org/2006/vcard/ns#Neighbor'"/>
        <xsl:variable name="vcard_None" select="'http://www.w3.org/2006/vcard/ns#None'"/>
        <xsl:variable name="vcard_Organization"
            select="'http://www.w3.org/2006/vcard/ns#Organization'"/>
        <xsl:variable name="vcard_Other" select="'http://www.w3.org/2006/vcard/ns#Other'"/>
        <xsl:variable name="vcard_PCS" select="'http://www.w3.org/2006/vcard/ns#PCS'"/>
        <xsl:variable name="vcard_Pager" select="'http://www.w3.org/2006/vcard/ns#Pager'"/>
        <xsl:variable name="vcard_Parcel" select="'http://www.w3.org/2006/vcard/ns#Parcel'"/>
        <xsl:variable name="vcard_Parent" select="'http://www.w3.org/2006/vcard/ns#Parent'"/>
        <xsl:variable name="vcard_Postal" select="'http://www.w3.org/2006/vcard/ns#Postal'"/>
        <xsl:variable name="vcard_Pref" select="'http://www.w3.org/2006/vcard/ns#Pref'"/>
        <xsl:variable name="vcard_RelatedType"
            select="'http://www.w3.org/2006/vcard/ns#RelatedType'"/>
        <xsl:variable name="vcard_Sibling" select="'http://www.w3.org/2006/vcard/ns#Sibling'"/>
        <xsl:variable name="vcard_Spouse" select="'http://www.w3.org/2006/vcard/ns#Spouse'"/>
        <xsl:variable name="vcard_Sweetheart"
            select="'http://www.w3.org/2006/vcard/ns#Sweetheart'"/>
        <xsl:variable name="vcard_Tel" select="'http://www.w3.org/2006/vcard/ns#Tel'"/>
        <xsl:variable name="vcard_TelephoneType"
            select="'http://www.w3.org/2006/vcard/ns#TelephoneType'"/>
        <xsl:variable name="vcard_Text" select="'http://www.w3.org/2006/vcard/ns#Text'"/>
        <xsl:variable name="vcard_TextPhone"
            select="'http://www.w3.org/2006/vcard/ns#TextPhone'"/>
        <xsl:variable name="vcard_Type" select="'http://www.w3.org/2006/vcard/ns#Type'"/>
        <xsl:variable name="vcard_Unknown" select="'http://www.w3.org/2006/vcard/ns#Unknown'"/>
        <xsl:variable name="vcard_VCard" select="'http://www.w3.org/2006/vcard/ns#VCard'"/>
        <xsl:variable name="vcard_Video" select="'http://www.w3.org/2006/vcard/ns#Video'"/>
        <xsl:variable name="vcard_Voice" select="'http://www.w3.org/2006/vcard/ns#Voice'"/>
        <xsl:variable name="vcard_Work" select="'http://www.w3.org/2006/vcard/ns#Work'"/>
        <xsl:variable name="vcard_X400" select="'http://www.w3.org/2006/vcard/ns#X400'"/>
        
        <!--properties-->
        <xsl:variable name="vcard_adr" select="'http://www.w3.org/2006/vcard/ns#adr'"/>
        <xsl:variable name="vcard_agent" select="'http://www.w3.org/2006/vcard/ns#agent'"/>
        <xsl:variable name="vcard_email" select="'http://www.w3.org/2006/vcard/ns#email'"/>
        <xsl:variable name="vcard_geo" select="'http://www.w3.org/2006/vcard/ns#geo'"/>
        <xsl:variable name="vcard_hasAdditionalName"
            select="'http://www.w3.org/2006/vcard/ns#hasAdditionalName'"/>
        <xsl:variable name="vcard_hasAddress"
            select="'http://www.w3.org/2006/vcard/ns#hasAddress'"/>
        <xsl:variable name="vcard_hasCalendarBusy"
            select="'http://www.w3.org/2006/vcard/ns#hasCalendarBusy'"/>
        <xsl:variable name="vcard_hasCalendarLink"
            select="'http://www.w3.org/2006/vcard/ns#hasCalendarLink'"/>
        <xsl:variable name="vcard_hasCalendarRequest"
            select="'http://www.w3.org/2006/vcard/ns#hasCalendarRequest'"/>
        <xsl:variable name="vcard_hasCategory"
            select="'http://www.w3.org/2006/vcard/ns#hasCategory'"/>
        <xsl:variable name="vcard_hasCountryName"
            select="'http://www.w3.org/2006/vcard/ns#hasCountryName'"/>
        <xsl:variable name="vcard_hasEmail"
            select="'http://www.w3.org/2006/vcard/ns#hasEmail'"/>
        <xsl:variable name="vcard_hasFN" select="'http://www.w3.org/2006/vcard/ns#hasFN'"/>
        <xsl:variable name="vcard_hasFamilyName"
            select="'http://www.w3.org/2006/vcard/ns#hasFamilyName'"/>
        <xsl:variable name="vcard_hasGender"
            select="'http://www.w3.org/2006/vcard/ns#hasGender'"/>
        <xsl:variable name="vcard_hasGeo" select="'http://www.w3.org/2006/vcard/ns#hasGeo'"/>
        <xsl:variable name="vcard_hasGivenName"
            select="'http://www.w3.org/2006/vcard/ns#hasGivenName'"/>
        <xsl:variable name="vcard_hasHonorificPrefix"
            select="'http://www.w3.org/2006/vcard/ns#hasHonorificPrefix'"/>
        <xsl:variable name="vcard_hasHonorificSuffix"
            select="'http://www.w3.org/2006/vcard/ns#hasHonorificSuffix'"/>
        <xsl:variable name="vcard_hasInstantMessage"
            select="'http://www.w3.org/2006/vcard/ns#hasInstantMessage'"/>
        <xsl:variable name="vcard_hasKey" select="'http://www.w3.org/2006/vcard/ns#hasKey'"/>
        <xsl:variable name="vcard_hasLanguage"
            select="'http://www.w3.org/2006/vcard/ns#hasLanguage'"/>
        <xsl:variable name="vcard_hasLocality"
            select="'http://www.w3.org/2006/vcard/ns#hasLocality'"/>
        <xsl:variable name="vcard_hasLogo" select="'http://www.w3.org/2006/vcard/ns#hasLogo'"/>
        <xsl:variable name="vcard_hasMember"
            select="'http://www.w3.org/2006/vcard/ns#hasMember'"/>
        <xsl:variable name="vcard_hasName" select="'http://www.w3.org/2006/vcard/ns#hasName'"/>
        <xsl:variable name="vcard_hasNickname"
            select="'http://www.w3.org/2006/vcard/ns#hasNickname'"/>
        <xsl:variable name="vcard_hasNote" select="'http://www.w3.org/2006/vcard/ns#hasNote'"/>
        <xsl:variable name="vcard_hasOrganizationName"
            select="'http://www.w3.org/2006/vcard/ns#hasOrganizationName'"/>
        <xsl:variable name="vcard_hasOrganizationUnit"
            select="'http://www.w3.org/2006/vcard/ns#hasOrganizationUnit'"/>
        <xsl:variable name="vcard_hasPhoto"
            select="'http://www.w3.org/2006/vcard/ns#hasPhoto'"/>
        <xsl:variable name="vcard_hasPostalCode"
            select="'http://www.w3.org/2006/vcard/ns#hasPostalCode'"/>
        <xsl:variable name="vcard_hasRegion"
            select="'http://www.w3.org/2006/vcard/ns#hasRegion'"/>
        <xsl:variable name="vcard_hasRelated"
            select="'http://www.w3.org/2006/vcard/ns#hasRelated'"/>
        <xsl:variable name="vcard_hasRole" select="'http://www.w3.org/2006/vcard/ns#hasRole'"/>
        <xsl:variable name="vcard_hasSound"
            select="'http://www.w3.org/2006/vcard/ns#hasSound'"/>
        <xsl:variable name="vcard_hasSource"
            select="'http://www.w3.org/2006/vcard/ns#hasSource'"/>
        <xsl:variable name="vcard_hasStreetAddress"
            select="'http://www.w3.org/2006/vcard/ns#hasStreetAddress'"/>
        <xsl:variable name="vcard_hasTelephone"
            select="'http://www.w3.org/2006/vcard/ns#hasTelephone'"/>
        <xsl:variable name="vcard_hasTitle"
            select="'http://www.w3.org/2006/vcard/ns#hasTitle'"/>
        <xsl:variable name="vcard_hasUID" select="'http://www.w3.org/2006/vcard/ns#hasUID'"/>
        <xsl:variable name="vcard_hasURL" select="'http://www.w3.org/2006/vcard/ns#hasURL'"/>
        <xsl:variable name="vcard_hasValue"
            select="'http://www.w3.org/2006/vcard/ns#hasValue'"/>
        <xsl:variable name="vcard_key" select="'http://www.w3.org/2006/vcard/ns#key'"/>
        <xsl:variable name="vcard_logo" select="'http://www.w3.org/2006/vcard/ns#logo'"/>
        <xsl:variable name="vcard_n" select="'http://www.w3.org/2006/vcard/ns#n'"/>
        <xsl:variable name="vcard_org" select="'http://www.w3.org/2006/vcard/ns#org'"/>
        <xsl:variable name="vcard_photo" select="'http://www.w3.org/2006/vcard/ns#photo'"/>
        <xsl:variable name="vcard_sound" select="'http://www.w3.org/2006/vcard/ns#sound'"/>
        <xsl:variable name="vcard_tel" select="'http://www.w3.org/2006/vcard/ns#tel'"/>
        <xsl:variable name="vcard_url" select="'http://www.w3.org/2006/vcard/ns#url'"/>
        <xsl:variable name="vcard_additional-name"
            select="'http://www.w3.org/2006/vcard/ns#additional-name'"/>
        <xsl:variable name="vcard_anniversary"
            select="'http://www.w3.org/2006/vcard/ns#anniversary'"/>
        <xsl:variable name="vcard_bday" select="'http://www.w3.org/2006/vcard/ns#bday'"/>
        <xsl:variable name="vcard_category"
            select="'http://www.w3.org/2006/vcard/ns#category'"/>
        <xsl:variable name="vcard_class" select="'http://www.w3.org/2006/vcard/ns#class'"/>
        <xsl:variable name="vcard_country-name"
            select="'http://www.w3.org/2006/vcard/ns#country-name'"/>
        <xsl:variable name="vcard_extended-address"
            select="'http://www.w3.org/2006/vcard/ns#extended-address'"/>
        <xsl:variable name="vcard_family-name"
            select="'http://www.w3.org/2006/vcard/ns#family-name'"/>
        <xsl:variable name="vcard_fn" select="'http://www.w3.org/2006/vcard/ns#fn'"/>
        <xsl:variable name="vcard_given-name"
            select="'http://www.w3.org/2006/vcard/ns#given-name'"/>
        <xsl:variable name="vcard_honorific-prefix"
            select="'http://www.w3.org/2006/vcard/ns#honorific-prefix'"/>
        <xsl:variable name="vcard_honorific-suffix"
            select="'http://www.w3.org/2006/vcard/ns#honorific-suffix'"/>
        <xsl:variable name="vcard_label" select="'http://www.w3.org/2006/vcard/ns#label'"/>
        <xsl:variable name="vcard_language"
            select="'http://www.w3.org/2006/vcard/ns#language'"/>
        <xsl:variable name="vcard_latitude"
            select="'http://www.w3.org/2006/vcard/ns#latitude'"/>
        <xsl:variable name="vcard_locality"
            select="'http://www.w3.org/2006/vcard/ns#locality'"/>
        <xsl:variable name="vcard_longitude"
            select="'http://www.w3.org/2006/vcard/ns#longitude'"/>
        <xsl:variable name="vcard_mailer" select="'http://www.w3.org/2006/vcard/ns#mailer'"/>
        <xsl:variable name="vcard_nickname"
            select="'http://www.w3.org/2006/vcard/ns#nickname'"/>
        <xsl:variable name="vcard_note" select="'http://www.w3.org/2006/vcard/ns#note'"/>
        <xsl:variable name="vcard_organization-name"
            select="'http://www.w3.org/2006/vcard/ns#organization-name'"/>
        <xsl:variable name="vcard_organization-unit"
            select="'http://www.w3.org/2006/vcard/ns#organization-unit'"/>
        <xsl:variable name="vcard_post-office-box"
            select="'http://www.w3.org/2006/vcard/ns#post-office-box'"/>
        <xsl:variable name="vcard_postal-code"
            select="'http://www.w3.org/2006/vcard/ns#postal-code'"/>
        <xsl:variable name="vcard_prodid" select="'http://www.w3.org/2006/vcard/ns#prodid'"/>
        <xsl:variable name="vcard_region" select="'http://www.w3.org/2006/vcard/ns#region'"/>
        <xsl:variable name="vcard_rev" select="'http://www.w3.org/2006/vcard/ns#rev'"/>
        <xsl:variable name="vcard_role" select="'http://www.w3.org/2006/vcard/ns#role'"/>
        <xsl:variable name="vcard_sort-string"
            select="'http://www.w3.org/2006/vcard/ns#sort-string'"/>
        <xsl:variable name="vcard_street-address"
            select="'http://www.w3.org/2006/vcard/ns#street-address'"/>
        <xsl:variable name="vcard_title" select="'http://www.w3.org/2006/vcard/ns#title'"/>
        <xsl:variable name="vcard_tz" select="'http://www.w3.org/2006/vcard/ns#tz'"/>
        <xsl:variable name="vcard_value" select="'http://www.w3.org/2006/vcard/ns#value'"/>
    </xsl:stylesheet>
    