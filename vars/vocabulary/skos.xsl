<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3">
    <xsl:variable name="_skos_origin"
        select="'https://www.w3.org/TR/2008/WD-skos-reference-20080829/skos.rdf'"/>
    
    <!--Classes-->
    <xsl:variable name="skos_Concept"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#Concept', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'Concept'}"/>
    <xsl:variable name="skos_ConceptScheme"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#ConceptScheme', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'ConceptScheme'}"/>
    <xsl:variable name="skos_Collection"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#Collection', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'Collection'}"/>
    <xsl:variable name="skos_OrderedCollection"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#OrderedCollection', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'OrderedCollection'}"/>
    
    <!--properties-->
    <xsl:variable name="skos_inScheme"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#inScheme', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'inScheme'}"/>
    <xsl:variable name="skos_hasTopConcept"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#hasTopConcept', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'hasTopConcept'}"/>
    <xsl:variable name="skos_topConceptOf"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#topConceptOf', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'topConceptOf'}"/>
    <xsl:variable name="skos_prefLabel"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#prefLabel', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'prefLabel'}"/>
    <xsl:variable name="skos_altLabel"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#altLabel', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'altLabel'}"/>
    <xsl:variable name="skos_hiddenLabel"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#hiddenLabel', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'hiddenLabel'}"/>
    <xsl:variable name="skos_notation"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#notation', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'notation'}"/>
    <xsl:variable name="skos_note"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#note', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'note'}"/>
    <xsl:variable name="skos_changeNote"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#changeNote', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'changeNote'}"/>
    <xsl:variable name="skos_definition"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#definition', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'definition'}"/>
    <xsl:variable name="skos_editorialNote"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#editorialNote', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'editorialNote'}"/>
    <xsl:variable name="skos_example"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#example', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'example'}"/>
    <xsl:variable name="skos_historyNote"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#historyNote', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'historyNote'}"/>
    <xsl:variable name="skos_scopeNote"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#scopeNote', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'scopeNote'}"/>
    <xsl:variable name="skos_semanticRelation"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#semanticRelation', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'semanticRelation'}"/>
    <xsl:variable name="skos_broader"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#broader', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'broader'}"/>
    <xsl:variable name="skos_narrower"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#narrower', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'narrower'}"/>
    <xsl:variable name="skos_related"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#related', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'related'}"/>
    <xsl:variable name="skos_broaderTransitive"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#broaderTransitive', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'broaderTransitive'}"/>
    <xsl:variable name="skos_narrowerTransitive"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#narrowerTransitive', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'narrowerTransitive'}"/>
    <xsl:variable name="skos_member"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#member', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'member'}"/>
    <xsl:variable name="skos_memberList"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#memberList', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'memberList'}"/>
    <xsl:variable name="skos_mappingRelation"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#mappingRelation', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'mappingRelation'}"/>
    <xsl:variable name="skos_broadMatch"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#broadMatch', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'broadMatch'}"/>
    <xsl:variable name="skos_narrowMatch"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#narrowMatch', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'narrowMatch'}"/>
    <xsl:variable name="skos_relatedMatch"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#relatedMatch', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'relatedMatch'}"/>
    <xsl:variable name="skos_exactMatch"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#exactMatch', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'exactMatch'}"/>
    <xsl:variable name="skos_closeMatch"
        select="map {'prefix' : 'skos', 'uri' : 'http://www.w3.org/2008/05/skos#closeMatch', 'ns' : 'http://www.w3.org/2008/05/skos#', 'local-name' : 'closeMatch'}"/>
</xsl:stylesheet>
