<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <!-- finnes ikke enda-->
    <xsl:variable name="_skosno_origin" select="'http://vokab.norge.no/skosno#'"/>
    
    <!-- classes-->
    <xsl:variable name="skosno_Definisjon" select="'http://vokab.norge.no/skosno#Definisjon'"/>
    <!-- variables-->
    <!-- skosno:alternativFormulering -->
    
    <xsl:variable name="skosno_bruksområde" select="'http://vokab.norge.no/skosno#bruksområde'"/>
    <xsl:variable name="skosno_datastrukturterm" select="'http://vokab.norge.no/skosno#datastrukturterm'"/> 
    <xsl:variable name="skosno_alternativFormulering" select="'http://vokab.norge.no/skosno#alternativFormulering'"/>
    <xsl:variable name="skosno_betydningsbeskrivelse" select="'http://vokab.norge.no/skosno#betydningsbeskrivelse'"/>
    
    <!-- instances-->
    <xsl:variable name="skosno_inst_allmennheten" select="'http://vokab.norge.no/skosno#allmennheten'"/>
    <xsl:variable name="skosno_inst_fagspesialist" select="'http://vokab.norge.no/skosno#fagspesialist'"/>
</xsl:stylesheet>