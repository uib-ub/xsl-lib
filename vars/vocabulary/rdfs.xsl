<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3">
    <xsl:variable name="_rdfs_origin"
        select="'https://www.w3.org/2000/01/rdf-schema.rdf'"/>
    
    <!--Classes-->
    <xsl:variable name="rdfs_Resource"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#Resource', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'Resource'}"/>
    <xsl:variable name="rdfs_Class"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#Class', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'Class'}"/>
    <xsl:variable name="rdfs_Literal"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#Literal', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'Literal'}"/>
    <xsl:variable name="rdfs_Container"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#Container', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'Container'}"/>
    <xsl:variable name="rdfs_ContainerMembershipProperty"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#ContainerMembershipProperty', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'ContainerMembershipProperty'}"/>
    <xsl:variable name="rdfs_Datatype"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#Datatype', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'Datatype'}"/>
    
    <!--properties-->
    <xsl:variable name="rdfs_subClassOf"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#subClassOf', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'subClassOf'}"/>
    <xsl:variable name="rdfs_subPropertyOf"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#subPropertyOf', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'subPropertyOf'}"/>
    <xsl:variable name="rdfs_comment"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#comment', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'comment'}"/>
    <xsl:variable name="rdfs_label"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#label', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'label'}"/>
    <xsl:variable name="rdfs_domain"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#domain', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'domain'}"/>
    <xsl:variable name="rdfs_range"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#range', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'range'}"/>
    <xsl:variable name="rdfs_seeAlso"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#seeAlso', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'seeAlso'}"/>
    <xsl:variable name="rdfs_isDefinedBy"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#isDefinedBy', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'isDefinedBy'}"/>
    <xsl:variable name="rdfs_member"
        select="map {'prefix' : 'rdfs', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#member', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'member'}"/>
</xsl:stylesheet>
