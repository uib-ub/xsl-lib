<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3">
    <xsl:variable name="_void_origin" select="'http://vocab.deri.ie/void.rdf'"/>
    
    <!--Classes--> 
    <xsl:variable name="void_Dataset"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#Dataset', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'Dataset'}"/>
    <xsl:variable name="void_Linkset"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#Linkset', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'Linkset'}"/>
    <xsl:variable name="void_TechnicalFeature"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#TechnicalFeature', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'TechnicalFeature'}"/>
    <xsl:variable name="void_DatasetDescription"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#DatasetDescription', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'DatasetDescription'}"/>
    
    <!--properties-->
    <xsl:variable name="void_feature"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#feature', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'feature'}"/>
    <xsl:variable name="void_subset"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#subset', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'subset'}"/>
    <xsl:variable name="void_target"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#target', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'target'}"/>
    <xsl:variable name="void_sparqlEndpoint"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#sparqlEndpoint', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'sparqlEndpoint'}"/>
    <xsl:variable name="void_linkPredicate"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#linkPredicate', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'linkPredicate'}"/>
    <xsl:variable name="void_exampleResource"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#exampleResource', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'exampleResource'}"/>
    <xsl:variable name="void_vocabulary"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#vocabulary', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'vocabulary'}"/>
    <xsl:variable name="void_subjectsTarget"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#subjectsTarget', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'subjectsTarget'}"/>
    <xsl:variable name="void_objectsTarget"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#objectsTarget', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'objectsTarget'}"/>
    <xsl:variable name="void_dataDump"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#dataDump', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'dataDump'}"/>
    <xsl:variable name="void_uriLookupEndpoint"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#uriLookupEndpoint', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'uriLookupEndpoint'}"/>
    <xsl:variable name="void_uriRegexPattern"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#uriRegexPattern', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'uriRegexPattern'}"/>
    <xsl:variable name="void_class"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#class', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'class'}"/>
    <xsl:variable name="void_classes"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#classes', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'classes'}"/>
    <xsl:variable name="void_classPartition"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#classPartition', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'classPartition'}"/>
    <xsl:variable name="void_distinctObjects"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#distinctObjects', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'distinctObjects'}"/>
    <xsl:variable name="void_distinctSubjects"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#distinctSubjects', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'distinctSubjects'}"/>
    <xsl:variable name="void_documents"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#documents', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'documents'}"/>
    <xsl:variable name="void_entities"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#entities', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'entities'}"/>
    <xsl:variable name="void_inDataset"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#inDataset', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'inDataset'}"/>
    <xsl:variable name="void_openSearchDescription"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#openSearchDescription', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'openSearchDescription'}"/>
    <xsl:variable name="void_properties"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#properties', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'properties'}"/>
    <xsl:variable name="void_property"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#property', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'property'}"/>
    <xsl:variable name="void_propertyPartition"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#propertyPartition', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'propertyPartition'}"/>
    <xsl:variable name="void_rootResource"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#rootResource', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'rootResource'}"/>
    <xsl:variable name="void_triples"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#triples', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'triples'}"/>
    <xsl:variable name="void_uriSpace"
        select="map {'prefix' : 'void', 'uri' : 'http://rdfs.org/ns/void#uriSpace', 'ns' : 'http://rdfs.org/ns/void#', 'local-name' : 'uriSpace'}"/>
</xsl:stylesheet>
