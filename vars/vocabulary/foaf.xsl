<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3">
    <xsl:variable name="_foaf_origin" select="'http://xmlns.com/foaf/spec/index.rdf'"/>
    
    <!--Classes-->
    <xsl:variable name="foaf_Class"
        select="map {'prefix' : 'foaf', 'uri' : 'http://www.w3.org/2000/01/rdf-schema#Class', 'ns' : 'http://www.w3.org/2000/01/rdf-schema#', 'local-name' : 'Class'}"/>
      <xsl:variable name="foaf_LabelProperty"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/LabelProperty', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'LabelProperty'}"/>
    <xsl:variable name="foaf_Person"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/Person', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'Person'}"/>
    <xsl:variable name="foaf_Document"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/Document', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'Document'}"/>
    <xsl:variable name="foaf_Organization"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/Organization', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'Organization'}"/>
    <xsl:variable name="foaf_Group"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/Group', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'Group'}"/>
    <xsl:variable name="foaf_Agent"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/Agent', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'Agent'}"/>
    <xsl:variable name="foaf_Project"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/Project', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'Project'}"/>
    <xsl:variable name="foaf_Image"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/Image', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'Image'}"/>

    
    <!--properties-->
    <xsl:variable name="foaf_mbox"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/mbox', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'mbox'}"/>
    <xsl:variable name="foaf_homepage"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/homepage', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'homepage'}"/>
    <xsl:variable name="foaf_weblog"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/weblog', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'weblog'}"/>
    <xsl:variable name="foaf_made"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/made', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'made'}"/>
    <xsl:variable name="foaf_maker"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/maker', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'maker'}"/>
    <xsl:variable name="foaf_knows"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/knows', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'knows'}"/>
    <xsl:variable name="foaf_primaryTopic"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/primaryTopic', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'primaryTopic'}"/>
    <xsl:variable name="foaf_isPrimaryTopicOf"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/isPrimaryTopicOf', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'isPrimaryTopicOf'}"/>
    <xsl:variable name="foaf_page"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/page', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'page'}"/>
    <xsl:variable name="foaf_member"
        select="map {'prefix' : 'foaf', 'uri' : 'http://xmlns.com/foaf/0.1/member', 'ns' : 'http://xmlns.com/foaf/0.1/', 'local-name' : 'member'}"/>
</xsl:stylesheet>
