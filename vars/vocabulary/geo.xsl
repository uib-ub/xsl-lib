<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3">
    <xsl:variable name="_geo_origin"
        select="'http://www.w3.org/2003/01/geo/wgs84_pos.rdf'"/>
    
    <!--Classes-->
    <xsl:variable name="geo_SpatialThing"
        select="map {'prefix' : 'geo', 'uri' : 'http://www.w3.org/2003/01/geo/wgs84_pos#SpatialThing', 'ns' : 'http://www.w3.org/2003/01/geo/wgs84_pos#', 'local-name' : 'SpatialThing'}"/>
    <xsl:variable name="geo_Point"
        select="map {'prefix' : 'geo', 'uri' : 'http://www.w3.org/2003/01/geo/wgs84_pos#Point', 'ns' : 'http://www.w3.org/2003/01/geo/wgs84_pos#', 'local-name' : 'Point'}"/>
    
    <!--properties-->
    <xsl:variable name="geo_lat"
        select="map {'prefix' : 'geo', 'uri' : 'http://www.w3.org/2003/01/geo/wgs84_pos#lat', 'ns' : 'http://www.w3.org/2003/01/geo/wgs84_pos#', 'local-name' : 'lat'}"/>
    <xsl:variable name="geo_location"
        select="map {'prefix' : 'geo', 'uri' : 'http://www.w3.org/2003/01/geo/wgs84_pos#location', 'ns' : 'http://www.w3.org/2003/01/geo/wgs84_pos#', 'local-name' : 'location'}"/>
    <xsl:variable name="geo_long"
        select="map {'prefix' : 'geo', 'uri' : 'http://www.w3.org/2003/01/geo/wgs84_pos#long', 'ns' : 'http://www.w3.org/2003/01/geo/wgs84_pos#', 'local-name' : 'long'}"/>
    <xsl:variable name="geo_alt"
        select="map {'prefix' : 'geo', 'uri' : 'http://www.w3.org/2003/01/geo/wgs84_pos#alt', 'ns' : 'http://www.w3.org/2003/01/geo/wgs84_pos#', 'local-name' : 'alt'}"/>
    <xsl:variable name="geo_lat_long"
        select="map {'prefix' : 'geo', 'uri' : 'http://www.w3.org/2003/01/geo/wgs84_pos#lat_long', 'ns' : 'http://www.w3.org/2003/01/geo/wgs84_pos#', 'local-name' : 'lat_long'}"/>
</xsl:stylesheet>
