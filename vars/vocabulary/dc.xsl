<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3">
    <xsl:variable name="_dc_origin"
        select="'http://dublincore.org/2012/06/14/dcelements.rdf'"/>
    
    <!--Classes-->
    
    
    <!--properties-->
    <xsl:variable name="dc_title"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/title', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'title'}"/>
    <xsl:variable name="dc_creator"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/creator', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'creator'}"/>
    <xsl:variable name="dc_subject"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/subject', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'subject'}"/>
    <xsl:variable name="dc_description"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/description', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'description'}"/>
    <xsl:variable name="dc_publisher"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/publisher', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'publisher'}"/>
    <xsl:variable name="dc_contributor"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/contributor', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'contributor'}"/>
    <xsl:variable name="dc_date"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/date', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'date'}"/>
    <xsl:variable name="dc_type"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/type', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'type'}"/>
    <xsl:variable name="dc_format"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/format', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'format'}"/>
    <xsl:variable name="dc_identifier"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/identifier', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'identifier'}"/>
    <xsl:variable name="dc_source"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/source', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'source'}"/>
    <xsl:variable name="dc_language"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/language', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'language'}"/>
    <xsl:variable name="dc_relation"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/relation', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'relation'}"/>
    <xsl:variable name="dc_coverage"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/coverage', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'coverage'}"/>
    <xsl:variable name="dc_rights"
        select="map {'prefix' : 'dc', 'uri' : 'http://purl.org/dc/elements/1.1/rights', 'ns' : 'http://purl.org/dc/elements/1.1/', 'local-name' : 'rights'}"/>
</xsl:stylesheet>
