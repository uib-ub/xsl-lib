<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:variable name="_skosxl_origin"
        select="'http://www.w3.org/TR/skos-reference/skos-xl.rdf'"/>
    
    <!--Classes-->
    <xsl:variable name="skosxl_Label" select="'http://www.w3.org/2008/05/skos-xl#Label'"/>
    
    <!--properties-->
    <xsl:variable name="skosxl_literalForm"
        select="'http://www.w3.org/2008/05/skos-xl#literalForm'"/>
    <xsl:variable name="skosxl_prefLabel"
        select="'http://www.w3.org/2008/05/skos-xl#prefLabel'"/>
    <xsl:variable name="skosxl_altLabel"
        select="'http://www.w3.org/2008/05/skos-xl#altLabel'"/>
    <xsl:variable name="skosxl_hiddenLabel"
        select="'http://www.w3.org/2008/05/skos-xl#hiddenLabel'"/>
    <xsl:variable name="skosxl_labelRelation"
        select="'http://www.w3.org/2008/05/skos-xl#labelRelation'"/>
</xsl:stylesheet>