<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3">
    <xsl:variable name="_rdf_origin"
        select="'https://www.w3.org/1999/02/22-rdf-syntax-ns.rdf'"/>
    
    <!--Classes-->
    <xsl:variable name="rdf_Property"
        select="map {'prefix' : 'rdf', 'uri' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#Property', 'ns' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'local-name' : 'Property'}"/>
    <xsl:variable name="rdf_Statement"
        select="map {'prefix' : 'rdf', 'uri' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#Statement', 'ns' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'local-name' : 'Statement'}"/>
    <xsl:variable name="rdf_Bag"
        select="map {'prefix' : 'rdf', 'uri' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#Bag', 'ns' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'local-name' : 'Bag'}"/>
    <xsl:variable name="rdf_Seq"
        select="map {'prefix' : 'rdf', 'uri' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#Seq', 'ns' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'local-name' : 'Seq'}"/>
    <xsl:variable name="rdf_Alt"
        select="map {'prefix' : 'rdf', 'uri' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#Alt', 'ns' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'local-name' : 'Alt'}"/>
    <xsl:variable name="rdf_List"
        select="map {'prefix' : 'rdf', 'uri' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#List', 'ns' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'local-name' : 'List'}"/>
    
    <!--properties-->
    <xsl:variable name="rdf_type"
        select="map {'prefix' : 'rdf', 'uri' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type', 'ns' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'local-name' : 'type'}"/>
    <xsl:variable name="rdf_subject"
        select="map {'prefix' : 'rdf', 'uri' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#subject', 'ns' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'local-name' : 'subject'}"/>
    <xsl:variable name="rdf_predicate"
        select="map {'prefix' : 'rdf', 'uri' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#predicate', 'ns' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'local-name' : 'predicate'}"/>
    <xsl:variable name="rdf_object"
        select="map {'prefix' : 'rdf', 'uri' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#object', 'ns' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'local-name' : 'object'}"/>
    <xsl:variable name="rdf_value"
        select="map {'prefix' : 'rdf', 'uri' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#value', 'ns' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'local-name' : 'value'}"/>
    <xsl:variable name="rdf_first"
        select="map {'prefix' : 'rdf', 'uri' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#first', 'ns' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'local-name' : 'first'}"/>
    <xsl:variable name="rdf_rest"
        select="map {'prefix' : 'rdf', 'uri' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#rest', 'ns' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'local-name' : 'rest'}"/>
</xsl:stylesheet>
