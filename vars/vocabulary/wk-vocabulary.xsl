<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="3.0">
    
    <!-- well known namespaces and vocabulary for autocomplete-->
    
    <!-- namespaces-->
    <xsl:variable name="void" select="'http://rdfs.org/ns/void#'" as="xs:string"/>
    <xsl:variable name="rdf" select="'http://www.w3.org/1999/02/22-rdf-syntax-ns#'"  as="xs:string"/>
    <xsl:variable name="rdfs" select="'http://www.w3.org/2000/01/rdf-schema#'"  as="xs:string"/>
    <xsl:variable name="dcterms" select="'http://purl.org/dc/terms/'" as="xs:string"/>
    <xsl:variable name="ecrm" select="'http://erlangen-crm.org/current/'"  as="xs:string"/>
    <xsl:variable name="foaf" select="'http://xmlns.com/foaf/0.1/'" as="xs:string"/>
    
 <!--   <!-\-well known properties-\-> 
    <xsl:variable name="rdf_type" select="xs:anyURI('http://www.w3.org/1999/02/22-rdf-syntax-ns#type')" as="xs:string"/>
    <xsl:variable name="rdfs_label" select="xs:anyURI('http://www.w3.org/2000/01/rdf-schema#label')" as="xs:string"/>
    
    
    <xsl:variable name="dcat_contactPoint" select="'http://www.w3.org/ns/dcat#contactPoint'"/>   
    -->
</xsl:stylesheet>