<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    
    exclude-result-prefixes="xs"
    version="3.0">
    <xsl:param name="prefix" as="xs:string" select="'foaf'"/>
    <xsl:param name="base-url" select="'http://xmlns.com/foaf/spec/index.rdf'"/>
    <!--only namespace as uri-prefix -->
    <xsl:param name="only-namespace" as="xs:string?" select="()"/>

    <xsl:output  indent="yes"/>
<xsl:variable name="double_spacing">
    <xsl:text>
                
</xsl:text>
</xsl:variable>
    <xsl:variable name="spacing">
 <xsl:text>
</xsl:text>
    </xsl:variable>
    <!-- meta script for generating vars from rdf/xml rdfs or owl for variables containing model-->
    
    <xsl:template match="/">
        <xsl:element name="xsl:stylesheet">
            <xsl:attribute name="version" select="'3'"/>
        <xsl:element name="xsl:variable">
            <xsl:attribute name="name" select="'_'||$prefix||'_origin'"/>
            <xsl:attribute name="select" select='"&apos;" || $base-url ||"&apos;"'/>
        </xsl:element>
            <xsl:value-of select="$double_spacing"/>
        <xsl:comment>Classes</xsl:comment>
            <xsl:value-of select="$spacing"/>
            <xsl:for-each select="*:RDF/*:Class[(not(@*:term_status) or @*:term_status='stable') and
                (empty($only-namespace) or starts-with(@rdf:about,$only-namespace))]
                ,rdf:RDF/*[(empty($only-namespace) or starts-with(@rdf:about,$only-namespace))][rdf:type/@rdf:resource=('http://www.w3.org/2000/01/rdf-schema#Class','http://www.w3.org/2002/07/owl#Class')]">
            <xsl:element name="xsl:variable">
                <xsl:attribute name="name" select="$prefix ||'_'|| tokenize(@*:about,'[/#]')[last()]"/>
                <xsl:call-template name="createVariableMapSelectAttribute"/>        
            </xsl:element>
        </xsl:for-each>
            <xsl:value-of select="$double_spacing"/>
            <!-- properties-->
            <xsl:comment select="'properties'"/><xsl:value-of select="$spacing"/>

            <xsl:for-each select="*:RDF/*[ends-with(name(),'Property') and not (contains(name(),'Annotation')) and (not(@*:term_status) or @*:term_status='stable') and (empty($only-namespace) or starts-with(@rdf:about,$only-namespace))],*:RDF/*[rdf:type/@rdf:resource='http://www.w3.org/1999/02/22-rdf-syntax-ns#Property' and (empty($only-namespace) or starts-with(@rdf:about,$only-namespace))]">
                <xsl:element name="xsl:variable">
                    <xsl:attribute name="name" select="$prefix || '_'|| tokenize(@*:about,'[/#]')[last()]"/>
                   <xsl:call-template name="createVariableMapSelectAttribute"/>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>              
    </xsl:template>
    
    <xsl:template name="createVariableMapSelectAttribute">
        <xsl:attribute name="select" select='"map {" || "&apos;prefix&apos; : &apos;" || $prefix ||
            "&apos;, &apos;uri&apos; : &apos;" || @*:about || 
            "&apos;, &apos;ns&apos; : &apos;" || replace(@*:about,"([/#])[^/#]+$","$1") ||  "&apos;, &apos;local-name&apos; : &apos;"|| tokenize(@*:about,"[/#]")[last()]||"&apos;}"'/>
    </xsl:template>
    
    <xsl:template match="*:RDF/*[ends-with(name(),'Property')]">
        
    </xsl:template>
    
    
    
</xsl:stylesheet>