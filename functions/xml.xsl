<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    exclude-result-prefixes="xs"
    version="3.0">    
    <xsl:param name="base-ns-ontology"/>
        
    <xsl:function name="flub:getbaseNSFromParamOrElement" as="xs:string">
        <xsl:param name="element" as="element(*)"/>
        <xsl:sequence select="($base-ns-ontology,namespace-uri-for-prefix('',$element))[string(.)][1]"></xsl:sequence>
    </xsl:function>    
    <xsl:function name="flub:wrapElements" >
        <xsl:param name="contents" as="item()*"/>
        
       <xsl:if test="exists($contents)">
        <wrapper>
            <xsl:sequence select="$contents"/>
        </wrapper>
       </xsl:if>      
        
    </xsl:function>
    
    <xsl:variable name="regex-xmlname" select="'^(([^:]+):(.+)|([^:]+))'"/>
    
    <!--replaces full string with '' if no prefix''-->
    <xsl:function name="flub:xmlnameGetPrefix" as="xs:string">
        <xsl:param name="xmlname" as="xs:string"/>
        <xsl:sequence select="replace($xmlname, $regex-xmlname, '$2','s')"/>
    </xsl:function>
    
    <xsl:function name="flub:xmlnameGetLocalName" as="xs:string">
        <xsl:param name="xmlname" as="xs:string"/>
        <xsl:sequence select="replace($xmlname,$regex-xmlname, '$3$4','s')"/>
    </xsl:function>
    
    <xsl:function name="flub:get-encoding" as="xs:string" visibility="private">
        <xsl:param name="document" as="document-node()"/>
        <xsl:variable name="encoding" as="xs:string?">            
            <xsl:analyze-string select="unparsed-text-lines(base-uri($document))[1]" regex='encoding="([^"]+)"'>
                <xsl:matching-substring>
                    <xsl:value-of select="regex-group(1)"/>
                </xsl:matching-substring>
            </xsl:analyze-string>
        </xsl:variable>
        <xsl:sequence select="if(string($encoding))
            then $encoding
            else 'UTF-8'"/>    
    </xsl:function>
    
    <xsl:function name="flub:entities-to-elements" as="document-node()">
        <xsl:param name="document" as="document-node()"/>
        <xsl:variable name="result">
            <xsl:analyze-string select="unparsed-text(base-uri($document),flub:get-encoding($document))" regex="&amp;([^; &amp;&quot;&apos;]+);">
                <xsl:matching-substring>
                    <xsl:text expand-text="1">&lt;flub:entity xmlns:flub="http://data.ub.uib.no/ns/function-library" name="{regex-group(1)}"/&gt;</xsl:text>
                </xsl:matching-substring>
                <xsl:non-matching-substring><xsl:value-of select="."/></xsl:non-matching-substring>
            </xsl:analyze-string>
        </xsl:variable>
        <xsl:sequence select="parse-xml($result)"/>
    </xsl:function>    
    
    <xsl:function name="flub:elements-to-entities" as="document-node()">
        <xsl:param name="document" as="document-node()"/>
        <xsl:apply-templates select="$document" mode="to-entities"/>
        
    </xsl:function>    
    
    <xsl:mode name="to-entities" on-no-match="shallow-copy"/>
    
    <xsl:template match="comment()" mode="to-entities">
        <xsl:choose>
            <xsl:when test="contains(.,'flub:entity')">

                    <xsl:variable name="context-node" select="/"></xsl:variable>
                <xsl:comment>
                <xsl:value-of>
               <xsl:analyze-string select="."  regex="(&lt;flub:entity.+?name=&quot;[^&quot;]+?&quot;/>)">
                        <xsl:matching-substring>
                            <xsl:message select="count(regex-group(1))||'matches '|| '&lt;_comment&gt;'||string(regex-group(1)) ||'&lt;/_comment&gt;'"></xsl:message>
                            <xsl:variable name="comment" as="xs:string" select="'&lt;_comment&gt;'||string(regex-group(1)) ||'&lt;/_comment&gt;'"/>
                           <xsl:for-each select="$context-node">
                               <xsl:try>
                                   <xsl:apply-templates select="parse-xml($comment)" mode="to-entities"/>
                                   <xsl:catch>
                                       <xsl:value-of select="$comment"/>
                                       <xsl:message select="'to entities parsing failed ' || $comment"></xsl:message>
                                   </xsl:catch>
                               </xsl:try>
                               
                           </xsl:for-each>
                            
                        </xsl:matching-substring>
                        <xsl:non-matching-substring>
                            <xsl:value-of select="."/>
                        </xsl:non-matching-substring>
                    </xsl:analyze-string>                
                    
                   
                    </xsl:value-of>
                    </xsl:comment> 
               
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy><xsl:apply-templates/></xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    

    <!-- dont copy _comment node-->
    <xsl:template match="*:_comment" mode="to-entities">
       <xsl:apply-templates mode="to-entities"/>        
    </xsl:template>
    
    <!--<xsl:mode name="to-entities" on-no-match="shallow-copy"/>-->
    
    <xsl:template match="flub:entity" mode="to-entities">
        <xsl:value-of select="'&amp;'||@name||';'" disable-output-escaping="true"/>
    </xsl:template>
</xsl:stylesheet>