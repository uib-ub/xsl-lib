<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    xmlns="http://www.w3.org/2005/xpath-functions"
    exclude-result-prefixes="xs math"
    version="3.0">
    
    <!-- override if not shared-->
    <xsl:variable name="lexicon-seed-prefix" select="'http://www.terminologi.no/ns/lexicon#'"/>
    
    
    <xsl:function name="flub:lexicon-seed" as="xs:string">
        <xsl:param name="xml-lang" as="attribute(xml:lang)"/>
        <xsl:if test="string-length($xml-lang)=0">
        <xsl:message terminate="yes">
            flub:lexicon-seed empty @xml:lang
        </xsl:message>
        </xsl:if>
        <xsl:text expand-text="1">{$lexicon-seed-prefix}{lower-case($xml-lang)}</xsl:text>
    </xsl:function>
    
    <xsl:variable name="language-codes-xml" select="json-to-xml(unparsed-text('https://datahub.io/core/language-codes/r/language-codes-full.json'))"/>
   
    
    <xsl:key name="language-from-code" match="map/string[@key='English']" use="parent::map/string[@key='alpha3-b' or @key='alpha2' or @key='alpha3-t']/@key"/>
    
    <xsl:function name="flub:language-from-xml-lang" as="xs:string">
      <xsl:param name="xml-lang" as="attribute(xml:lang)"/>   
        
        <xsl:sequence select="key('language-from-code',lower-case($xml-lang),$language-codes-xml)"></xsl:sequence>
    
    </xsl:function>
    
    
    
    <xsl:template match="/">
        
        <xsl:variable name="xml-lang" as="attribute(xml:lang)">
            <xsl:attribute name="xml:lang" select="'no'"/>
        </xsl:variable>
        <xsl:sequence select="flub:language-from-xml-lang($xml-lang)"/>
        <xsl:sequence select="$language-codes-xml"/>
        
    </xsl:template>
</xsl:stylesheet>