<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://exslt.org/math"
    xmlns:ubbgeo="http://ub.uib.no/geo"    
    exclude-result-prefixes="xs"
    version="2.0">
    
    
    
    
    <xsl:variable name="ubbgeo:ellipsoid">
        <_ellipsoid>                         
<row>        <id>1</id><name> "Airy"</name><equatorial_radius>6377563</equatorial_radius><square_of_eccentricity>0.00667054</square_of_eccentricity>,</row>
<row>        <id>2</id><name> "Australian National"</name><equatorial_radius>6378160</equatorial_radius><square_of_eccentricity>0.006694542</square_of_eccentricity>,</row>
<row>        <id>3</id><name> "Bessel 1841"</name><equatorial_radius>6377397</equatorial_radius><square_of_eccentricity>0.006674372</square_of_eccentricity>,</row>
<row>        <id>4</id><name> "Bessel 1841 (Nambia] "</name><equatorial_radius>6377484</equatorial_radius><square_of_eccentricity>0.006674372</square_of_eccentricity>,</row>
<row>        <id>5</id><name> "Clarke 1866"</name><equatorial_radius>6378206</equatorial_radius><square_of_eccentricity>0.006768658</square_of_eccentricity>,</row>
<row>        <id>6</id><name> "Clarke 1880"</name><equatorial_radius>6378249</equatorial_radius><square_of_eccentricity>0.006803511</square_of_eccentricity>,</row>
<row>        <id>7</id><name> "Everest"</name><equatorial_radius>6377276</equatorial_radius><square_of_eccentricity>0.006637847</square_of_eccentricity>,</row>
<row>        <id>8</id><name> "Fischer 1960 (Mercury] "</name><equatorial_radius>6378166</equatorial_radius><square_of_eccentricity>0.006693422</square_of_eccentricity>,</row>
<row>        <id>9</id><name> "Fischer 1968"</name><equatorial_radius>6378150</equatorial_radius><square_of_eccentricity>0.006693422</square_of_eccentricity>,</row>
<row>        <id>10</id><name> "GRS 1967"</name><equatorial_radius>6378160</equatorial_radius><square_of_eccentricity>0.006694605</square_of_eccentricity>,</row>
<row>        <id>11</id><name> "GRS 1980"</name><equatorial_radius>6378137</equatorial_radius><square_of_eccentricity>0.00669438</square_of_eccentricity>,</row>
<row>        <id>12</id><name> "Helmert 1906"</name><equatorial_radius>6378200</equatorial_radius><square_of_eccentricity>0.006693422</square_of_eccentricity>,</row>
<row>        <id>13</id><name> "Hough"</name><equatorial_radius>6378270</equatorial_radius><square_of_eccentricity>0.00672267</square_of_eccentricity>,</row>
<row>        <id>14</id><name> "International"</name><equatorial_radius>6378388</equatorial_radius><square_of_eccentricity>0.00672267</square_of_eccentricity>,</row>
<row>        <id>15</id><name> "Krassovsky"</name><equatorial_radius>6378245</equatorial_radius><square_of_eccentricity>0.006693422</square_of_eccentricity>,</row>
<row>        <id>16</id><name> "Modified Airy"</name><equatorial_radius>6377340</equatorial_radius><square_of_eccentricity>0.00667054</square_of_eccentricity>,</row>
<row>        <id>17</id><name> "Modified Everest"</name><equatorial_radius>6377304</equatorial_radius><square_of_eccentricity>0.006637847</square_of_eccentricity>,</row>
<row>        <id>18</id><name> "Modified Fischer 1960"</name><equatorial_radius>6378155</equatorial_radius><square_of_eccentricity>0.006693422</square_of_eccentricity>,</row>
<row>        <id>19</id><name> "South American 1969"</name><equatorial_radius>6378160</equatorial_radius><square_of_eccentricity>0.006694542</square_of_eccentricity>,</row>
<row>        <id>20</id><name> "WGS 60"</name><equatorial_radius>6378165</equatorial_radius><square_of_eccentricity>0.006693422</square_of_eccentricity>,</row>
<row>        <id>21</id><name> "WGS 66"</name><equatorial_radius>6378145</equatorial_radius><square_of_eccentricity>0.006694542</square_of_eccentricity>,</row>
<row>        <id>22</id><name> "WGS-72"</name><equatorial_radius>6378135</equatorial_radius><square_of_eccentricity>0.006694318</square_of_eccentricity></row>
<row>        <id>23</id><name> "WGS-84"</name><equatorial_radius>6378137</equatorial_radius><square_of_eccentricity>0.00669438</square_of_eccentricity>      </row>
        </_ellipsoid>
    </xsl:variable>
  
    <xsl:function name="ubbgeo:UTMtoLL">
        <xsl:param name="ReferenceEllipsoid" as="xs:double"/>
        <xsl:param name="UTMNorthing" as="xs:double"/>
        <xsl:param name="UTMEasting" as="xs:double"/>
        <xsl:param name="UTMhemis"/>
        <xsl:param name="UTMzone"/>
        
        <!--XSLT translation Oyvind Gjesdal oyvind.gjesdal@ub.uib.no 
            Translated from Python:
                 - Translation (converted to Python by Russ Nelson (nelson@crynwr.com)
                 - Originally written by Chuck Gantz- chuck.gantz@globalstar.com     
           #converts UTM coords to lat/long.  
           Equations from USGS Bulletin 1532 
        #East Longitudes are positive, West longitudes are negative. 
        #North latitudes are positive, South latitudes are negative
        #Lat and Long are in decimal degrees. 
        #Written by Chuck Gantz- chuck.gantz@globalstar.com
        #Converted to Python by Russ Nelson nelson@crynwr.com-->
            
            <xsl:variable name="k0" select="0.9996"/>         
            <xsl:variable name="a" select="$ubbgeo:ellipsoid/descendant::row[$ReferenceEllipsoid]/descendant::equatorial_radius cast as xs:double"/>
            <xsl:variable name="eccSquared" select="$ubbgeo:ellipsoid/descendant::row[$ReferenceEllipsoid]/descendant::square_of_eccentricity cast as xs:double"/>
            
            <xsl:variable name="e1" select="(1-math:sqrt(1-$eccSquared)) div (1+math:sqrt(1-$eccSquared))"/>
      <!--      #NorthernHemisphere; //1 for northern hemispher, 0 for southern-->
            
            <xsl:variable name="x" select="$UTMEasting - 500000.0"/><!-- #remove 500,000 meter offset for longitude-->
            <xsl:variable name="y" select="if($UTMhemis ne 'N') then $UTMNorthing -10000000.0 else $UTMNorthing"/>
            
            <xsl:variable name="ZoneLetter" select="$UTMhemis"/>
            <xsl:variable name="ZoneNumber" select="$UTMzone"/>
          
            <xsl:variable name="LongOrigin" select="($ZoneNumber - 1)*6 - 180 + 3"/>  <!--# +3 puts origin in middle of zone-->
            
            <xsl:variable name="eccPrimeSquared" select="($eccSquared) div (1-$eccSquared)"/>
            
            <xsl:variable name="M" select="$y div $k0"/>
            <xsl:variable name="mu" select="$M div ($a*(1-$eccSquared div 4-3*$eccSquared*$eccSquared div 64-5*$eccSquared*$eccSquared*$eccSquared div 256))"/>
            
            <xsl:variable name="phi1Rad" select="($mu + (3*$e1 div 2-27*$e1*$e1*$e1 div 32)*math:sin(2*$mu) 
            + (21*$e1*$e1 div 16-55*$e1*$e1*$e1*$e1 div 32)*math:sin(4*$mu)
            +(151*$e1*$e1*$e1 div 96)*math:sin(6*$mu))"/>
                
                
        <xsl:variable name="_rad2deg" select="180.0 div math:constant('PI',12)"/>
        
            <xsl:variable name="phi1" select="$phi1Rad*$_rad2deg"/>
            
            <xsl:variable name="N1" select="$a div math:sqrt(1-$eccSquared*math:sin($phi1Rad)*math:sin($phi1Rad))"/>
            <xsl:variable name="T1" select="math:tan($phi1Rad)*math:tan($phi1Rad)"/>
            <xsl:variable name="C1" select="$eccPrimeSquared*math:cos($phi1Rad)*math:cos($phi1Rad)"/>
            <xsl:variable name="R1" select="$a*(1-$eccSquared) div math:power(1-$eccSquared*math:sin($phi1Rad)*math:sin($phi1Rad), 1.5)"/>
            <xsl:variable name="D" select="$x div ($N1*$k0)"/>
            
            <xsl:variable name="Lat" select= "$phi1Rad - ($N1*math:tan($phi1Rad) div $R1)*($D*$D div 2-(5+3*$T1+10*$C1 -4*$C1*$C1 -9*$eccPrimeSquared)*$D*$D*$D*$D div 24
            +(61+90*$T1+298*$C1+45*$T1*$T1 -252*$eccPrimeSquared -3*$C1*$C1)*$D*$D*$D*$D*$D*$D div 720)"/>
            <xsl:variable name="Latitude" select="$Lat * $_rad2deg"/>
            
            <xsl:variable name="Long" select="($D -(1+2*$T1+$C1)*$D*$D*$D div 6+(5-2*$C1+28*$T1 -3*$C1*$C1+8*$eccPrimeSquared+24*$T1*$T1)
            *$D*$D*$D*$D*$D div 120) div math:cos($phi1Rad)"/>
            <xsl:variable name="Longitude" select="$LongOrigin + $Long * $_rad2deg"/>
        
        <xsl:sequence select="concat($Latitude,' ',$Longitude)"/>
  
   </xsl:function>

</xsl:stylesheet>

