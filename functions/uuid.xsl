<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    xmlns:ubbef="http://data.ub.uib.no/ns/saxon/extension-functions#"
    xmlns:uuid="java.util.UUID"
    exclude-result-prefixes="xs"
    version="3.0">
    <!-- requires saxon PE or EE-->
    
    <xsl:function name="flub:generateUUID">   
        <xsl:value-of select="uuid:randomUUID()" use-when="function-available('uuid:randomUUID') and not(function-available('ubbef:random-uuid'))"/>
        <xsl:value-of select="ubbef:random-uuid()" use-when="function-available('ubbef:random-uuid')"/>
    </xsl:function>
    
    <xsl:function name="flub:uuid-from-string">
        <xsl:param name="string" as="xs:string"/>
        <xsl:value-of select="ubbef:uuid-from-string($string)" use-when="function-available('ubbef:uuid-from-string')"/>
        <xsl:value-of select="uuid:fromString($string)" use-when="function-available('uuid:fromString') 
            and not(function-available('ubbef:uuid-from-string'))"/>
        
        <xsl:message select="'flub:uuid-from-string missing extension function ubbef:uuid-from-string or uuid:fromString(PE or EE Saxon)'"
            use-when="not(function-available('ubbef:uuid-from-string')) and not(function-available('uuid:fromString'))"/>
    </xsl:function>
    
    <xsl:function name="flub:isValidUUID" as="xs:boolean">
        <xsl:param name="value" as="xs:string"/>
        <xsl:try select="if (string(flub:uuid-from-string($value))) then true() else false()">
            <xsl:catch>
                <xsl:sequence select="false()"/>
            </xsl:catch>
        </xsl:try>
    </xsl:function>
    
    <xsl:function name="flub:isOpenRefineSHA" as="xs:boolean">
        <xsl:param name="value" as="xs:string"/>
        <xsl:sequence select="if (matches($value,'^[0-9a-f]{40}$')) then true() else false()"></xsl:sequence>
    </xsl:function>
     
    <!-- parsing a uuid from zero or more multiple tokens in attribute-->
    <!-- @todo legg inn lovlig uuid uten prefix,dersom det kun er en token i attributt-->
    <xsl:function name="flub:getUUID">
        <xsl:param name="input"/>
        <xsl:if test="not(matches($input,'uuid:'))">
        <xsl:message terminate="yes">
            funksjon flub:getUUID krever uuid: som del av inputstreng.
        </xsl:message>
        </xsl:if>
        <xsl:for-each select="tokenize($input,'\s')">
           <xsl:if test="matches(.,'uuid:')">
            <xsl:value-of select="substring-after(.,'uuid:')"/>
           </xsl:if>
        </xsl:for-each>
    </xsl:function>
</xsl:stylesheet>