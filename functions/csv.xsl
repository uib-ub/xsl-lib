<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:array="http://www.w3.org/2005/xpath-functions/array"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    xmlns:error="http://data.ub.uib.no/ns/function-library/error"
    exclude-result-prefixes="xs math"
    version="3.0">
    
    
    <xsl:variable name="escape-regex-csv"><xsl:text>([&quot;])</xsl:text></xsl:variable>
    <xsl:variable name="csv-separator" select="','"/>
    
    
    <xsl:function name="flub:csv-line" as="xs:string">
        <xsl:param name="header" as="array(xs:string?)"/>
        <xsl:param name="fields" as="map(xs:string,xs:string?)"/>        
        <xsl:if test="map:size($fields) != array:size($header)">
        <xsl:message terminate="yes"/>            
        </xsl:if>
        
        <xsl:variable name="header-size" select="array:size($header)"/>
    <xsl:variable name="items" as="xs:string+">
    <xsl:for-each select="1 to $header-size">
        <xsl:text expand-text="1">"{replace(map:get($fields,array:get($header,.)),$escape-regex-csv,'$1$1')}"{if (.!=$header-size) then $csv-separator else ''}</xsl:text>
    </xsl:for-each>    
    </xsl:variable>
        
        <xsl:sequence select="string-join($items,'')"/>
        
    </xsl:function>
    

</xsl:stylesheet>