<xsl:stylesheet xmlns="http://www.w3.org/2005/xpath-functions"
    xmlns:json="http://www.w3.org/2005/xpath-functions"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:array="http://www.w3.org/2005/xpath-functions/array"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3.0" exclude-result-prefixes="#all">
    <xsl:param name="debug" as="xs:boolean" select="true()" static="1"/>
    <!-- required (must) parts terminate the application if not present -->   
    
    <xsl:variable name="default-service-type" select="'ImageService2'"/>
    <xsl:variable name="defult-profile" select="'level1'"/>
    
    <xsl:variable name="prezi-doc" select="json-to-xml(unparsed-text('prezi.json'))" as="document-node()"/>
    
    <xsl:variable name="collection-json" select="json-doc('collection.json')"/>
    <xsl:variable name="collection-doc" select="json-to-xml(unparsed-text('collection.json'))"/>
    <xsl:function name="flub:iiif_service" as="map(xs:string,xs:string)">
        <xsl:param name="service" as="map(xs:string,xs:string)"/>
        
      <xsl:sequence select="map {'@id' : exactly-one(map:get($service,'id')),
          '@type' : (map:get($service,'type')[string(.)],$default-service-type)[1],
          'profile' : (map:get($service,'profile')[string(.)],$defult-profile)[1]}"/>        
    </xsl:function>
    
   <xsl:function name="flub:iiif_thumbnail" as="element(json:array)">
       <xsl:param name="thumbnail" as="map(*)"/>
       <xsl:variable name="thumbnail-map" select="map {'thumbnail' : 
           [ map {'id' : map:get($thumbnail,'id'),
          'service' : map:get($thumbnail,'service')
          ,'type' : 'Image'}]}"/>
       <xsl:sequence select="json-to-xml(serialize($thumbnail-map, map {'method' : 'json'}))/descendant-or-self::json:array[1]"/>
   </xsl:function>
    
    <!-- should -->
    <xsl:template match="/json:map/json:array[@key='provider']">
        
    </xsl:template>
    
    <xsl:template match="/json:map/json:string[@key='id']|/json:map/json:map[@key='label']|/json:map/json:array[@key='metadata']|/json:map/json:array[@key='items']|/json:map/json:string[@key='rights']|json:map/json:array[@key='thumbnail']" mode="prezi">
        <xsl:message terminate="yes" select="name() || '/@key=' || @key || ' must be overridden'"/>
    </xsl:template>
    
    <xsl:template match="json:null" mode="prezi">
        <xsl:message use-when="$debug" select="@key || ' optional not set'"/>
    </xsl:template>
    
    
    <xsl:function name="flub:iiif_label" as="element(json:map)">
        <xsl:param name="labels" as="map(xs:string,array(xs:string))"/>
        <map key="label">
            <xsl:for-each select="map:keys($labels)">                
                <xsl:if test=". != 'none' and not(. castable as xs:language)">
                    <xsl:message select="'illegal language'" terminate="yes"/>
                </xsl:if>
                    <array key="{.}">
                        <xsl:variable name="strings" select="map:get($labels,.)" as="array(xs:string)"/>
                        <xsl:for-each select="for $x in 1 to array:size($strings) return
                            array:get($strings,$x)">
                            <string>
                                <xsl:value-of select="."/>
                            </string>
                        </xsl:for-each>
                    </array>
            </xsl:for-each>
        </map>
    </xsl:function>
    
     <xsl:function name="flub:iiif_collection">
        <xsl:param name="collection" as="map(*)"/>      
       
        
        <xsl:sequence select="map {
            '@context' : exactly-one($collection-json('@json')),
            'id' : exactly-one($collection('id')),
            'type' : 'Collection',
            'label' : exactly-one($collection('label')),
            'summary': ($collection('summary'),$collection-json('summary'))[1],
            'requiredStatement' : ($collection('requiredStatement'),$collection-json('requiredStatement'))[1],
            'items' : exactly-one($collection('items'))}"/>
        
    </xsl:function>
    
    <xsl:function name="flub:item-type-by-uri">
        <xsl:param name="uri" as="xs:string"/>        
        <xsl:analyze-string select="$uri" regex="^https?://[^/]+/iiif/((manifest)|(collection))/.+$">
            <xsl:matching-substring>
                <xsl:sequence select="if (string(regex-group(2))) then 'Manifest' else 'Collection'"/>
            </xsl:matching-substring>
            <xsl:non-matching-substring>
                <xsl:message select="'type for uri not recognized'" terminate="yes"/>
            </xsl:non-matching-substring>
        </xsl:analyze-string>
    </xsl:function>
    
    <xsl:function name="flub:iiif_collection_items">
        <xsl:param name="items" as="map(*)*"/>
        
        <xsl:for-each select="map:keys($items)">
            <xsl:variable name="type" select="flub:item-type-by-uri(.)"/>
            <xsl:sequence select="[ for $x in map:keys($items) return map {
                'id' : $x,
                'type' : flub:item-type-by-uri($x),
                'label' : $items($x)('label'),
                'thumbnail' : $items($x)('thumbnail')
                }
                ]"></xsl:sequence>
        </xsl:for-each>
        
    </xsl:function>
    
    <xsl:function name="flub:iiif_embedded-canvas" as="element(json:map)">
        <xsl:param name="canvas" as="map(*)"/>
        <xsl:param name="debug" as="xs:string"/>
        <xsl:variable name="image-uri" select="$canvas('image-uri')" as="xs:string"/>
        <xsl:variable name="tokens-image-uri" select="tokenize($image-uri,'/')"/><!--1,2,3=base uri,etterpå er slug-->        
        <xsl:variable name="base-uri" select="string-join($tokens-image-uri[position()=1 to 3],'/')"/>        
        <xsl:variable name="id-part" select="string-join($tokens-image-uri[position()=4 to last()],'/')"/>
        <xsl:variable name="canvas-uri" select="($canvas('uri'),$base-uri || '/canvas/' || $id-part)[1]" as="xs:string"/>
        <xsl:variable name="annotationpage-uri" select="($canvas('ap-uri'),$base-uri || '/annotationpage/' || $id-part)[1]" as="xs:string"/> 
        <xsl:variable name="annotation-uri" select="($canvas('a-uri'),$base-uri || '/annotation/' || $id-part)[1]" as="xs:string"/>
        <xsl:variable name="canvas-label" select="($tokens-image-uri[last()],$canvas('label'))[1]" as="xs:string"/>
        <xsl:variable name="info-json-uri" select="$image-uri || '/info.json'"/>
        <xsl:variable name="json-doc" as="map(*)">
            <xsl:try select="json-doc($info-json-uri)">
                <xsl:catch>
                    <xsl:message select="$info-json-uri || ' finnes ikke ' || $debug || ' side' "/>                    
                    <xsl:sequence select="map{}"/>
                </xsl:catch>
            </xsl:try>
        </xsl:variable>
        <xsl:variable name="height" select="$json-doc('height')"/>
        <json:map>
        <xsl:choose>
            <xsl:when test="map:size($json-doc) > 0">
 <json:string key="type"><xsl:value-of select="'Canvas'"/></json:string>     
       <json:string key="id"><xsl:value-of select="$canvas-uri"/></json:string>
       <xsl:sequence select="flub:iiif_label(map {'none': [ $canvas-label ] })"/>
            <json:number key="width"><xsl:value-of select="$json-doc('width')"/></json:number>
	    <json:number key="height"><xsl:value-of select="$json-doc('height')"/></json:number>
	    <json:string key="target"><xsl:value-of select="$canvas-uri"/></json:string>
            <json:array key="items">
            <json:map>
            <json:string key="id"><xsl:sequence select="$annotationpage-uri"/></json:string>
            <json:string key="type">AnnotationPage</json:string>
             <json:array key="items">
                <json:map>
                    <json:string key="id"><xsl:value-of select="$annotation-uri"/></json:string>
                    <json:string key="type">Annotation</json:string>
                    <json:string key="target"><xsl:value-of select="$canvas-uri"/></json:string>
                    <json:map key="body">
                        <json:string key="id"><xsl:value-of select="$canvas('image-uri')"/></json:string>
                        <json:string key="type">Image</json:string>
                        <json:string key="format">image/jpeg</json:string>
                        <json:number key="width"><xsl:value-of select="exactly-one($json-doc('height'))"/></json:number>
                        <json:number key="height"><xsl:value-of select="exactly-one($json-doc('width'))"/></json:number>
                    <json:array key="service">
                            <json:map>                             
                            <json:string key="@type">ImageService2</json:string>
                            <json:string key="profile">level1</json:string>
                            <json:string key="@id"><xsl:value-of select="$image-uri"/></json:string>
                            </json:map>           
                        </json:array>
                    </json:map>
                    <json:string key="motivation">painting</json:string>
                </json:map>
            </json:array>
        </json:map>
        </json:array>
            </xsl:when>
            <xsl:otherwise>
                <json:string key="error"><xsl:value-of select="$canvas-label || 'id' || $canvas('image-uri')"/></json:string>
            </xsl:otherwise>
        </xsl:choose>
        </json:map>
        
        
    </xsl:function>
    <xsl:function name="flub:iiif_metadata" as="element(json:array)">
        <xsl:param name="metadata" as="array(map(xs:string,map(xs:string,array(xs:string))))"/>
     
        <xsl:variable name="metadata-e" select="json-to-xml(serialize(map {'metadata': $metadata }, map {'method': 'json'}))/descendant::*:array[1]" as="element(json:array)"/>
            <xsl:apply-templates select="$metadata-e" mode="iiif_metadata"/>        
            
    </xsl:function>
    <xsl:template mode="iiif_metadata" match="*[parent::json:array[@key='metadata']]">
        <xsl:if test="some $x in (json:map[@key='value']/json:array[@key]/json:string) satisfies string-length($x) > 0">
            <xsl:next-match/>
        </xsl:if>
    </xsl:template>
    
    <xsl:mode name="iiif_metadata" on-no-match="shallow-copy"/>
    
</xsl:stylesheet>
