<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    xmlns:ris="http://data.ub.uib.no/ns/function-library/ris"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"    
    xmlns:array="http://www.w3.org/2005/xpath-functions/array"
    xmlns:if_p3="http://data.ub.uib.no/ns/function-library/iiif-presentation-3"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:variable name="context-uri" select="'http://iiif.io/api/presentation/3/context.json'"/>
    <xsl:variable name="context-doc" select="json-to-xml(unparsed-text($context-uri))" as="document-node()"/>
    <xsl:variable name="default-options">
        <xsl:sequence select="map {'@context' : $context-uri}"></xsl:sequence>
    </xsl:variable>
    
    <xsl:function name="if_p3:language-map" as="map(xs:string,array(xs:string))">
        <xsl:param name="language-map" as="map(xs:string,array(xs:string))"/>
        <xsl:variable name="languages" select="map:keys($language-map)" />
        <xsl:if test="not(every $x in $languages satisfies $x castable as xs:language)">
            <xsl:variable name="error_description" as="xs:string"><xsl:text expand-text="1">invalid language value in ({$languages}), language value should be a value castable as xs:language, use &apos;none&apos;' if language is not applicable.</xsl:text></xsl:variable>
            <xsl:sequence select="error(xs:QName('if_p3:error1'),$error_description)"/>
        </xsl:if>
        <xsl:sequence select="$language-map"/>
    </xsl:function>
        
    <xsl:function name="if_p3:metadata">
        <xsl:param name="metadata" as="map(xs:string,array(*))"/>       
        <xsl:variable name="metadata-parent" as="array(map(*)?)" select="map:get($metadata,'metadata')" />
        <xsl:variable name="has-metadata" as="xs:boolean" select="if (array:size($metadata-parent) &gt; 0) then true() else false()"/>
        <xsl:variable name="metadata-items" as="map(*)*" select="if ($has-metadata) 
            then (for $x in 1 to array:size($metadata-parent) return array:get($metadata-parent,$x))
            else ()"/>
        <!-- -->
        <xsl:variable name="labels"
            select="''"/>
        
        <xsl:variable name="values"/>
        <xsl:choose>
            <!-- checked in $metadata-parent that the one map:key of input is 'metadata' -->
            <xsl:when test="map:size($metadata)!=1">
                <xsl:sequence select="error(xs:QName('if_p3:error2'),'The value of the metadata property must be an array of JSON objects')"/>
            </xsl:when>
            <xsl:when test="not(every $x in $metadata-items satisfies (map:contains($x,'label') and map:contains($x,'value')) and map:size($x)=2)">
                
            </xsl:when>
        </xsl:choose>
        
        <xsl:sequence select="$metadata"/>
    </xsl:function>
    
<!--    <xsl:function name="if_p3:metadata_section">
        <xsl:param name="metadata" as="map(*)+"/>
        <xsl:sequence select="map {'metadata' :  [ $metadata ]}"/>
    </xsl:function>    -->
 <!--   
    <xsl:function name="if_p3:collection">
        <xsl:param name="id" as="xs:string"/>
        <xsl:param name="label-language-map" as="map(*)+"/>
        <xsl:param name="summary-language-map" as="map(*)*"/>
        <xsl:param name="required-statement-metadata-map" as="map(*)?"/>
        <xsl:param name="items" as="array(map(*))"/>
        
        <xsl:variable name="id"/>
        <xsl:variable name="label" select="map {'label' : $label-language-map}" as="map(xs:string,map(xs:string, "/>
        
        <xsl:map>
            <xsl:map-entry key="'context'" select="'http://iiif.io/api/presentation/3/context.json'"/>
        </xsl:map>
        
        
        <xsl:sequence select="map {
            'context' : '',
            'id' : $id,
            'type' : 'Collection',
            if ($summary-language-map) then 'summary' : $summary-language-map) else (), }"></xsl:sequence>
    </xsl:function>
  -->  
<!--    <xsl:function name="if_p3:metadata">
        <xsl:param name="label-language-map" as="map(xs:string,array(xs:string))+"/>
        <xsl:param name="value-language-map" as="map(xs:string,array(xs:string))+"/>
        
        <xsl:sequence select="map { 'label' : $label-language-map,
            'value' : $value-language-map}"/>
    </xsl:function>-->
    
</xsl:stylesheet>