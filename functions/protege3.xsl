<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:flub="http://data.ub.uib.no/ns/function-library" version="3.0">
    <xsl:variable name="ubbinst" select="'http://data.ub.uib.no/instance/'"/>
    
    <xsl:function name="flub:getInstancePrefix">
        <xsl:param name="class" as="map(*)"/>
        
        <xsl:variable name="uri" as="xs:string" select="map:get($class,'uri')"/>
        <xsl:if test="not($uri castable as xs:anyURI)">
            <xsl:message terminate="yes">illegal uri </xsl:message>
        </xsl:if>
        <xsl:variable name="uri">
            <xsl:choose>
                <xsl:when test="$uri='http://www.w3.org/2004/02/skos/core#Concept'">
                    <xsl:value-of select="'http://data.ub.uib.no/topic/'"/>
                </xsl:when>
                <xsl:when test="$uri='http://www.w3.org/2004/02/skos/core#ConceptScheme'">
                    <xsl:value-of select="'http://data.ub.uib.no/conceptscheme/'"/>
                </xsl:when>
                <xsl:when test="$uri='http://data.ub.uib.no/ontology/ProxyCollection'">
                    <xsl:value-of select="'http://data.ub.uib.no/instance/collection/'"/>
                </xsl:when>
                <xsl:when test="$uri='http://data.ub.uib.no/ontology/Exhibition'">
                    <xsl:value-of select="'http://data.ub.uib.no/exhibition/'"/>
                </xsl:when>
                <xsl:when test="matches($uri,'/$')">
                    <xsl:variable name="tokens" select="tokenize($uri,'/')" as="xs:string+"/>
                    <xsl:if test="count($tokens) &lt; 4">
                        <xsl:message terminate="yes">
                            a uri without a local part cannot be used as class name due to naming. <xsl:value-of select="$uri"/>
                        </xsl:message>
                    </xsl:if>
                    <xsl:value-of select="concat($ubbinst,$tokens[last()-1]) || '/'"/>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="concat($ubbinst,tokenize(replace($uri,'^.+?/([^/]+)$','$1'),'#')[last()]) || '/'"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:sequence select="lower-case($uri)"/>
    </xsl:function>
    
</xsl:transform>