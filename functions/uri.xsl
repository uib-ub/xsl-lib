<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    exclude-result-prefixes="xs math"
    version="3.0">
    
    <!-- override base-ns i importing stylesheet -->
    <xsl:variable name="base-ns" select="'http://terminologi.no/id/'"/>
    <xsl:function name="flub:uri-from-slug" as="xs:string">
        <xsl:param name="slug" as="xs:string"/>        
        <xsl:if test="string-length($slug)=0">
            <xsl:message terminate="yes">flub:uri-from-slug requires a slug-length greater than 0</xsl:message>
        </xsl:if>        
       <xsl:text expand-text="1">{$base-ns}{$slug}</xsl:text>     
    </xsl:function>
</xsl:stylesheet>