<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:flub="http://data.ub.uib.no/ns/function-library" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ubbef="http://data.ub.uib.no/ns/saxon/extension-functions#" version="3.0">
    
    <!--<xsl:param name="debug" select="false()" static="yes"/>-->
    
    <xsl:variable name="coordinate-type-from-name" as="map(*)" select="map {
        'Byggningscentroide, Språksamlingane': 'Bygningscentroide',
        'Bygningscentroide, Språksamlingane': 'Bygningscentroide',
        'Gardnummercentroide, Språksamlingane': 'Gardnummercentroide',
        'Hovedteigcentroide, Språksamlingane': 'Kartverket Hovedteigcentroide'}
        "/>
    <xsl:variable name="autoritets-urier" as="map(xs:string,map(xs:string, xs:string))">
        <xsl:map>
            <xsl:for-each select="$steds-autoritets-id, $koordinatTyper">
                <xsl:variable name="lookup-identifier" as="xs:boolean"
                    select="map:contains($coordinate-type-from-name, .)"/>
                <xsl:variable name="identifier"
                    select="
                        if ($lookup-identifier) then
                            map:get($coordinate-type-from-name, .)
                        else
                            ."/>
                <xsl:variable name="query">
                    <xsl:text expand-text="1">PREFIX rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#>
PREFIX dct: &lt;http://purl.org/dc/terms/>
SELECT distinct ?s WHERE {{
  graph ?g {{
    ?s ?p ?o.
    ?s dct:identifier "{$identifier}" }}.
}}</xsl:text>
                </xsl:variable>
                <xsl:message select="'identifier: ' || ." use-when="$debug"/>
                <xsl:message select="flub:sparqlSelect($query, $autoritets-endpoint)"
                    use-when="$debug"/>
                <xsl:map-entry key="string(.)">
                    <xsl:sequence
                        select="flub:nquri((string(exactly-one(flub:sparqlSelect($query, $autoritets-endpoint)/descendant::*:uri)), 'false')[1])"
                    />
                </xsl:map-entry>
            </xsl:for-each>
        </xsl:map>
    </xsl:variable>
    <!-- X = longitude Y latitude {"ID":"128","Gardsnamn":"Vik østre","Gardsnamn_SNID":"SNID-NO-0008600114740","Bruksnamn":"Vik østre","Bruksnamn_SNID":"SNID-NO-0008600114740","Kjelde":"Matrikkel","År":"1886","Fylke":"Smaalenenes Amt (Østfold)","Kommune":"Berg","MIDu":"010100360003","GNIDu":"01010036","HID":"0116-36/3","KNR":"0116","GNR":"36","BNR":"3","CRS":"EPSG:4326","X":"11.2654609833574","Y":"59.139637712112","Koordinattype":"Kartverket Adressepunkt","Matrikkelinformasjon":"https://seeiendom.kartverket.no/eiendom/0101/36/3/0/0","Lenke til digital matrikkel":"https://www.rhd.uit.no/matrikkel/mtliste.aspx?knr=0116&gnr=36"} -->
    
    <xsl:function name="flub:place-resources" as="map(xs:string,map(*))">
        <xsl:param name="place" as="map(*)"/>
        <xsl:param name="KNR" as="xs:string"/>
        
        <xsl:variable name="KNR" select="$place($KNR)" as="xs:string"/>
        <xsl:variable name="KNR-seed" select="$bustadnamn-named-graph || 'KNR_' || $KNR" as="xs:string"/>
        <xsl:variable name="herred-uuid" select="ubbef:uuid-from-string($KNR-seed)" as="xs:string"/>        
        <xsl:variable name="herred-uri" select="$stadnamn-prefix || $herred-uuid" as="xs:string"/>        
        <xsl:variable name="FNR" select="substring($KNR,1,2)" as="xs:string"/>
        <xsl:variable name="FNR-seed" select="$bustadnamn-named-graph || 'FNR_' || $FNR" as="xs:string"/>
        <xsl:variable name="fylke-uuid" select="ubbef:uuid-from-string($FNR-seed)"/>
        <xsl:variable name="fylke-uri" select="$stadnamn-prefix || $fylke-uuid " as="xs:string"/>        
        
        <xsl:sequence select="map {$KNR : 
            map {
            'fylke-uri' : $fylke-uri,
            'fylke-uuid' : $fylke-uuid,
            'herred-uri' : $herred-uri,
            'herred-uuid' : $herred-uuid}}"/>        
        
    </xsl:function>
    
    <xsl:function name="flub:place-resources">
        <xsl:param name="place" as="map(*)"/>
        <xsl:sequence select="flub:place-resources($place,'KNR')"/>
    </xsl:function>
    
    
</xsl:stylesheet>