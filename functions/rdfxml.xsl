<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    exclude-result-prefixes="xs math map rdf flub math"
    version="3.0" expand-text="1">
    
    <xsl:param name="legacy-xsd-datatype" as="xs:boolean" select="false()"/>
    <xsl:include href="rdf-datatypes.xsl"/>
    <!-- Choosing not to do validation of subject predicate object as uris.  Use riot -validate step in ci pipeline to validate output-->
    <!-- Could use absolute-uri regex from http://www.w3.org/2011/04/XMLSchema/TypeLibrary-URI-RFC3986.xsd and http://www.w3.org/2011/04/XMLSchema/TypeLibrary-IRI-RFC3987.xsd and assert if matches regex-->
    <!-- blank nodes not implemented-->
    
    <xsl:variable name="xsd_string" select="$xsd||'string'"/>
    <xsl:function name="flub:rdf-description" as="element(rdf:Description)">
        <xsl:param name="resource" as="xs:string"/>
        <rdf:Description rdf:about="{$resource}"/>
    </xsl:function>
    
    <xsl:function name="flub:rdf-xml-object" visibility="public">
        <xsl:param name="description" as="element(rdf:Description)"/>
        <xsl:param name="predicate-as-prefix-name" as="map(xs:string,xs:string)"/>
        <xsl:param name="object" as="map(xs:string,xs:string)*"/>        
        <xsl:variable name="children" select="$description/child::*" as="element(*)*"/>
        <rdf:Description rdf:about="{$description/@rdf:about}">
        <xsl:iterate select="$object">
            <xsl:param name="predicates" as="element(*)*" select="$children"/>
            <xsl:on-completion>
                <xsl:sequence select="$predicates"/>
            </xsl:on-completion>
            <xsl:variable name="url" select="map:get(.,'uri')" as="xs:string"/>
            <xsl:variable name="predicate" as="element(*)">
                <xsl:element name="{map:get($predicate-as-prefix-name,'prefix')}:{map:get($predicate-as-prefix-name,'local-name')}" namespace="{map:get($predicate-as-prefix-name,'ns')}">
                <xsl:attribute name="rdf:resource" select="$url"/>
            </xsl:element>
            </xsl:variable>
            <xsl:next-iteration>
                <xsl:with-param name="predicates" select="flub:unique-predicates($predicates,$predicate)"/>
            </xsl:next-iteration>
        </xsl:iterate>
        </rdf:Description>
    </xsl:function>
    
    <xsl:function name="flub:rdf-xml-literal" visibility="public" as="element(rdf:Description)">
        <xsl:param name="description" as="element(rdf:Description)"/>
        <xsl:param name="predicate-as-prefix-name" as="map(xs:string,xs:string)"/>
        <xsl:param name="object" as="map(xs:string,xs:string)*"/>        
        <xsl:variable name="children" as="element(*)*" select="$description/child::*" />
        <rdf:Description rdf:about="{$description/@rdf:about}">            
        <xsl:iterate select="$object">
            <xsl:param name="predicates" as="element(*)*" select="$children"/>
            <!-- exactly one key and value-->
            <xsl:on-completion>
                <xsl:sequence select="$predicates"/>
            </xsl:on-completion>            
            
            <xsl:variable name="this-predicate-literal" as="element(*)">
                <xsl:variable name="datatype" select="if (map:contains(.,'datatype'))
                    then map:get(.,'datatype')
                    else ()" as="xs:string?"/>
                <xsl:variable name="language" select="if (map:contains(.,'lang'))
                    then map:get(.,'lang')
                    else ()" as="xs:string?"/>
                <xsl:variable name="value" select="map:get(.,'value')" as="xs:string"/>
         
                <xsl:element name="{map:get($predicate-as-prefix-name,'prefix')}:{map:get($predicate-as-prefix-name,'local-name')}" namespace="{map:get($predicate-as-prefix-name,'ns')}" >
                <xsl:if test="$datatype and not($language)">                
                    <xsl:attribute name="rdf:datatype" select="$datatype"/>                    
                </xsl:if>
                
                <xsl:choose>
                    <xsl:when test="$language and ($datatype!=$xsd_string) and string($datatype)">
                        <xsl:sequence select="error()"/>
                    </xsl:when>
                </xsl:choose>
                
                <!-- add datatype string declaration if legacy mode (old jena serialization in Protege) and datatype is not set or set to string-->
                <xsl:if test="$legacy-xsd-datatype and not($language) and ($datatype=$xsd_string or not($datatype))">
                    <xsl:attribute name="rdf:datatype" select="$xsd_string"/>
                </xsl:if>
                    
                <xsl:if test="$language">
                    <xsl:sequence select="if (not($language castable as xs:language))
                        then error()
                        else ()"/>
                    <xsl:attribute name="xml:lang" select="$language"/>
                </xsl:if>             
                    <xsl:sequence select="$value"/>                
            </xsl:element>
            </xsl:variable>
            <xsl:next-iteration>
                <xsl:with-param name="predicates" select="flub:unique-predicates($predicates,$this-predicate-literal)"/>
            </xsl:next-iteration>
        </xsl:iterate>            
        </rdf:Description>   
    </xsl:function>
    
    <xsl:function name="flub:unique-predicates" as="element(*)*" visibility="private">
        <xsl:param name="predicates" as="element(*)*"/>
        <xsl:param name="predicate" as="element(*)"/>
        <xsl:sequence select="if (every $x in $predicates satisfies not(deep-equal($x,$predicate))) 
            then ($predicates,$predicate)
            else $predicates"/>        
    </xsl:function>
    
</xsl:stylesheet>
