<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    xmlns:ubbont="http://data.ub.uib.no/ontology/"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    exclude-result-prefixes="xs rdf owl rdfs map"
    version="3.0">    
    <xsl:include href="xml.xsl"/>
    <xsl:key name="object-property" match="owl:ObjectProperty" use="@rdf:about"/>
    <xsl:key name="datatype-property" match="owl:DatatypeProperty" use="@rdf:about"/>
    <xsl:key name="object-property" match="owl:FunctionalProperty[rdf:type/@rdf:resource='http://www.w3.org/2002/07/owl#ObjectProperty']" use="@rdf:about"/>
    <xsl:key name="datatype-property" match="owl:FunctionalProperty[rdf:type/@rdf:resource='http://www.w3.org/2002/07/owl#DatatypeProperty']" use="@rdf:about"/>
    <xsl:key name="property" match="owl:DatatypeProperty | owl:ObjectProperty | owl:FunctionalProperty | owl:DeprecatedProperty |owl:SymmetricProperty | owl:TransitiveProperty|owl:AnnotationProperty" use="@rdf:about"/>
    
    <xsl:key name="class" match="rdfs:Class|owl:Class" use="@rdf:about"/>
    
    <xsl:output method="xml" indent="yes"> </xsl:output>
    <!-- override ontology by import precedence-->
    <xsl:variable name="this">
        <root/>
    </xsl:variable>
    
    <xsl:variable name="ontology" select="document('http://data.ub.uib.no/ontology/ubbont.owl')"/>
    <xsl:variable name="namespace" select="'http://data.ub.uib.no/'"/>
    <!-- @todo ønsker vi å bruke flere rdf:type på et tidspunkt?, gir feilmelding dersom det er mer eller mindre enn en type på samme subject innenfor en context-->
    <xsl:function name="flub:getTypeFromStrings">
        <xsl:param name="string" as="xs:string*"/>
        <xsl:variable name="number-of-strings" select="count($string)" as="xs:integer"/>
        <xsl:choose>
            <xsl:when test="$number-of-strings = 1">
                <xsl:sequence select="$string"/></xsl:when>
            
            <xsl:otherwise>
                <xsl:message>flub:getTypeFromStrings Type not found: <xsl:sequence
                    select="
                    if (count($number-of-strings) > 1) then
                    'more than one type'
                    else
                    'no type defined'"
                /></xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <!-- from ontology-->
    <xsl:function name="flub:translateXmlNametoURI">
        <xsl:param name="xmlname" as="xs:string"/>
        <xsl:choose>
            <xsl:when test="matches(distinct-values($xmlname), '^http://')">
                <xsl:value-of select="$xmlname"/>
             <!--   <xsl:message>flub:translateXmlNameToUri: <xsl:value-of select="$xmlname"/> xmlname
                    is already uri, returning as is.</xsl:message>-->
            </xsl:when>
            <xsl:otherwise>
                              
                <xsl:value-of
                    select="concat(flub:lookupNamespaceUriInOntology($xmlname), flub:xmlnameGetLocalName($xmlname))"
                />
                
            </xsl:otherwise>
        </xsl:choose>
       
    </xsl:function>
    <xsl:variable name="known-props" select="'rdfs:seeAlso','rdfs:label','owl:imports','rdf:type','owl:versionInfo'" as="xs:string+"/>
    
    <xsl:function name="flub:isClassInOntology">
        <xsl:param name="string" as="xs:string"/>
        <xsl:variable name="class-uri" select="flub:translateXmlNametoURI($string)"/>
        <xsl:variable name="class" select="key('class', $class-uri, $ontology)"/>
        <xsl:if test="not($class)">
            <xsl:message terminate="yes">flub:isClassInOntology: <xsl:value-of select="$class-uri"/> not found in ontology </xsl:message>
        </xsl:if>
        <xsl:sequence
            select="
            if ($class)
            then
            true()
            else
            false()"
        />
    </xsl:function>
    
    <!-- also checks class-->
    <xsl:function name="flub:lookupPropertyInOntology" as="xs:anyURI">
        <xsl:param name="string"/>
        <xsl:variable name="property-uri" select="flub:translateXmlNametoURI($string)"/>
        
        <xsl:if test="not(key('property', $property-uri, $ontology)) and not(key('class',$property-uri,$ontology)) and not($string=$known-props)">
            <xsl:message>property: <xsl:value-of select="$string"/> not found in ontology</xsl:message>
        </xsl:if>
        <xsl:sequence select="xs:anyURI($property-uri)"/>
    </xsl:function>
    
    <xsl:function name="flub:isObjectProperty" as="xs:boolean">
        <xsl:param name="property"/>
        <xsl:variable name="property-uri" select="flub:lookupPropertyInOntology($property)"/>
        <xsl:sequence
            select="
            if (key('object-property', $property-uri, $ontology)) then
            true()
            else
            false()"
        />
    </xsl:function>
    
    <xsl:function name="flub:isProperty">
        <xsl:param name="property"/>
        <xsl:sequence
            select="
            if (flub:isObjectProperty($property) or flub:isdatatypeProperty($property))
            then
            true()
            else
            false()"
        />
    </xsl:function>
    
    <xsl:function name="flub:isdatatypeProperty" as="xs:boolean">
        <xsl:param name="property"/>
        <xsl:variable name="property-uri" select="flub:lookupPropertyInOntology($property)"/>
        
        <xsl:sequence
            select="
            if (key('datatype-property', $property-uri, $ontology)) then
            true()
            else
            false()"
        />
    </xsl:function>
    
    <xsl:variable name="namespace-lookup" as="map(xs:string,xs:string)">
       
        <xsl:iterate select="($ontology/descendant-or-self::rdf:RDF/namespace::*,/rdf:RDF/namespace::*)">
            <xsl:param name="map" as="map(xs:string,xs:string)">
                <xsl:map/>
            </xsl:param>
            <xsl:on-completion> <xsl:message select="concat('mmo: ',map:get($map,'mmo'))"/>
            <xsl:sequence select="$map"></xsl:sequence></xsl:on-completion>
           
            <xsl:choose>
                <xsl:when test="map:contains($map,local-name(.))">
                    <xsl:if test="map:get($map,local-name(.))!= .">
                        <xsl:message expand-text="1">mismatch namespace {local-name(.)}, {map:get($map,local-name(.))} , {.} </xsl:message>
                    </xsl:if>
                    <xsl:next-iteration/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:next-iteration>
                        <xsl:with-param name="map" select="map:put($map,local-name(.),string(.))"/>
                    </xsl:next-iteration>                    
                </xsl:otherwise>
            </xsl:choose>
        </xsl:iterate>
        
    </xsl:variable>
  
    <!-- @todo handle no namespace and default lookup-->
    <xsl:function name="flub:lookupNamespaceUriInOntology" as="xs:string">
        <xsl:param name="string" as="xs:string"/>
        <xsl:variable name="namespace"
            select="map:get($namespace-lookup,flub:xmlnameGetPrefix($string))"/>
        <xsl:if test="not(string($namespace)) and not($string=('noPrefix',''))">
            <xsl:message terminate="yes"><xsl:sequence select="flub:xmlnameGetPrefix($string)"></xsl:sequence>unused namespace in <xsl:value-of select="$string"
            /></xsl:message>
        </xsl:if>       
        <xsl:value-of select="$namespace"/>
      
    </xsl:function>
    
    <xsl:function name="flub:predicatehasInverseProperty" as="xs:boolean">
        <xsl:param name="predicate"/>
        <xsl:variable name="property-name" select="flub:lookupPropertyInOntology($predicate)"/>
        <xsl:variable name="property" select="key('property',$property-name,$ontology)"/>
        <xsl:sequence select="if (($property)/owl:inverseOf) then true() else false()"/>
    </xsl:function>    
    
    <xsl:function name="flub:getInverseProperty">
        <xsl:param name="predicate"/>
        <xsl:variable name="property" select="distinct-values(key('property',
            flub:lookupPropertyInOntology($predicate),
            $ontology)/owl:inverseOf/(owl:ObjectProperty/@rdf:about,@rdf:resource))"/>
        
        <xsl:if test="count($property) > 1"><xsl:message select="concat('property ',string-join($property,', '))">  </xsl:message></xsl:if> 
        <xsl:analyze-string select="$property" regex="^(.+[#/])([^#/]+$)">
            <xsl:matching-substring>
                <xsl:sequence select="regex-group(1),regex-group(2)"></xsl:sequence>
            </xsl:matching-substring>
            <xsl:non-matching-substring>
                <xsl:message terminate="yes">
                    <xsl:value-of select="'NAMESPACE NOT HANDLED flub:getInverseProperty'"/>
                </xsl:message>
            </xsl:non-matching-substring>
        </xsl:analyze-string>
        
    </xsl:function>
    
    <xsl:function name="flub:getRangeFromPredicate">
        <xsl:param name="predicate"/>
        <xsl:value-of select="key('property',flub:lookupPropertyInOntology($predicate),$ontology)/rdfs:range/@rdf:resource"/>
    </xsl:function>
    
    
    <xsl:variable name="prefix-uri-exceptions">
        <exceptions>
            <class type="http://www.w3.org/2004/02/skos/core#Concept" prefix="{concat($namespace,'topic/')}"/>
            <class type="http://www.w3.org/2004/02/skos/core#ConceptScheme" prefix="{concat($namespace,'conceptscheme/')}"/>
            <class type="http://data.ub.uib.no/ontology/Exhibition" prefix="{concat($namespace,'exhibition/')}"/>
        </exceptions>
    </xsl:variable>
    
    <xsl:key name="exception-prefix" match="*:class/@prefix" use="parent::*:class/@type"/>
    
         <!-- sets a uri based on :
         presence in uri-exceptions,
         instance/{lower-case(property-stubname)}-->
    <xsl:function name="flub:setSubjectUriFromClassAndValue">
        <xsl:param name="class-name"/>
        <xsl:param name="stub"/>
        
        <xsl:variable name="class-uri" select="flub:lookupPropertyInOntology($class-name)"/>
        <xsl:variable name="local-name-regex" select="'^.+[/#]([^/^#]+)$'"/>
        <xsl:if test="not(matches($class-uri,$local-name-regex))">
            <xsl:message terminate="yes">flub:setSubjectUriFromClassAndValue terminated: Invalid <xsl:value-of select="$class-uri"/></xsl:message>
        </xsl:if>
        <xsl:variable name="class-local-name" select="replace($class-uri,$local-name-regex,'$1')"/>
        <!-- Endrer fra class label til local name, siden uri er mer bestandig enn en label satt på en uri-->
        <!--<xsl:variable name="class-eng-label" select="key('class',$class-uri,$ontology)/rdfs:label[@xml:lang='en']"/>
        <xsl:if test="not($class-eng-label)">
            <xsl:message terminate="yes">
                <xsl:message>error in ontology, needs english rdfs:label to create uri.</xsl:message>
            </xsl:message>
        </xsl:if>-->
        <xsl:variable name="stub" select="encode-for-uri($stub)"/>
        <xsl:variable name="class-uri-name" select="lower-case($class-local-name)"/>
        <xsl:variable name="exception-prefix" select="key('exception-prefix',$class-uri,$prefix-uri-exceptions)"/>
        <xsl:choose>
            <xsl:when test="$exception-prefix">
                <xsl:value-of select="concat($exception-prefix,$stub)"/>
            </xsl:when>
            <!--<xsl:when test="$class-uri='http://www.w3.org/2004/02/skos/core#Concept'">
                <xsl:value-of select="concat($namespace,'topic/',$stub)"/>
            </xsl:when>
            <xsl:when test="$class-uri='http://www.w3.org/2004/02/skos/core#ConceptScheme'">
                <xsl:value-of select="concat($namespace,'conceptscheme/',$stub)"/>
            </xsl:when>
            <xsl:when test="$class-uri='http://data.ub.uib.no/ontology/Exhibition'">
                <xsl:value-of select="concat($namespace,'exhibition/',$stub)"/>
            </xsl:when>        -->          
            <!-- basic handling, override $namespace in importing stylesheet-->
            <xsl:otherwise>
                <xsl:value-of select="concat($namespace,'instance/',$class-uri-name,'/',$stub)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
</xsl:stylesheet>