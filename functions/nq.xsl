<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    exclude-result-prefixes="xs math"
    version="3.0" expand-text="1">
    
    <xsl:param name="default-named-graph" as="xs:string?" select="()" static="1"/>
    <!-- Choosing not to do validation of subject predicate object as uris.  Use riot -validate step in ci pipeline to validate output-->
    <!-- Could use absolute-uri regex from http://www.w3.org/2011/04/XMLSchema/TypeLibrary-URI-RFC3986.xsd and http://www.w3.org/2011/04/XMLSchema/TypeLibrary-IRI-RFC3987.xsd and assert if matches regex-->
    <!-- blank nodes not implemented-->
    <xsl:function name="flub:nqobject" as="xs:string">
        <xsl:param name="subject" as="xs:string"/>
        <xsl:param name="predicate" as="map(xs:string,xs:string)"/>
        <xsl:param name="object" as="map(xs:string,xs:string)*"/>
        <xsl:param name="graph" as="xs:string?"/>
                
        <xsl:value-of select="concat(string-join(for $x in $object return flub:nqwrite($subject,$predicate('uri'),concat('&lt;',$x('uri'),'&gt;'),$graph),''),'')"/>
    </xsl:function>
    
    <xsl:function name="flub:nqobject" as="xs:string">
        <xsl:param name="subject" as="xs:string"/>
        <xsl:param name="predicate" as="map(xs:string,xs:string)"/>
        <xsl:param name="object" as="map(xs:string,xs:string)*"/>
        
        
        <xsl:choose>
            <xsl:when test="not(string($default-named-graph))">
                <xsl:value-of select="string-join(flub:nqobject($subject,$predicate,$object,()),'')" use-when="not(string($default-named-graph))"/>
            </xsl:when>
            <!--@todo change signature for 4 params -->
            <xsl:otherwise>
                <xsl:sequence select="flub:nqobject($subject,$predicate,$object,$default-named-graph)"></xsl:sequence>
            </xsl:otherwise>
        </xsl:choose>
        
       
    </xsl:function>
    
    <xsl:output method="text"/>
    
    <xsl:variable name="xsd" select="'http://www.w3.org/2001/XMLSchema#'"/>

    <!-- map of literal key "value" - "literal
        and "datatype" "uri" "lang"        where datatype is optional-->    
    
    <xsl:function name="flub:boolean-literal" as="map(xs:string,xs:string)?">
        <xsl:param name="value" as="xs:boolean?"/>
        <xsl:if test="exists($value)">
        <xsl:map>        
            <xsl:map-entry key="'value'" select="string($value)"/> 
            <xsl:map-entry select="concat($xsd,'boolean')" key="'datatype'"/>          
        </xsl:map>
        </xsl:if>
    </xsl:function>
    
    <xsl:function name="flub:string" as="map(xs:string,xs:string)?">
        <xsl:param name="value" as="xs:string?"/>
    <xsl:sequence select="flub:string($value,())"/>
    </xsl:function>
    
    <xsl:function name="flub:string" as="map(xs:string,xs:string)?">
        <xsl:param name="value" as="xs:string?"/>
        <xsl:param name="lang" as="xs:string?"/>
        <xsl:if test="$value">
            <xsl:map>
                <xsl:map-entry key="'value'" select="$value"/>
                <xsl:if test="$lang">
                    <xsl:map-entry key="'lang'" select="$lang"/>
                </xsl:if>
            </xsl:map>
        </xsl:if>
    </xsl:function>
   
    <xsl:function name="flub:nqliteral" visibility="public" as="xs:string*">
        <xsl:param name="subject" as="item()*"/>
        <xsl:param name="predicate" as="map(xs:string,xs:string)"/>
        <xsl:param name="object" as="map(xs:string,xs:string)*"/>
        
        <xsl:choose>
            <xsl:when test="not(string($default-named-graph))">
                <xsl:for-each select="$subject">
                    <xsl:value-of select="string-join(for $x in $object return flub:nqliteral(.,$predicate,$x,()),'')"/>                    
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:sequence select="flub:nqliteral($subject,$predicate,$object,$default-named-graph)"/>
            </xsl:otherwise>
        </xsl:choose>     
    </xsl:function>
    
    <xsl:function name="flub:nquri" as="map(xs:string,xs:string)*">
        <xsl:param name="uri" as="xs:string*"/>
        <xsl:sequence select="for $x in $uri return map {'uri' : $x }"/>
    </xsl:function>
    
    <xsl:function name="flub:nqliteral" visibility="public" as="xs:string*">
        <xsl:param name="subject" as="item()"/>
        <xsl:param name="predicate" as="map(xs:string,xs:string)"/>
        <xsl:param name="object" as="map(xs:string,xs:string)*"/>
        <xsl:param name="graph" as="xs:string?"/>
     <xsl:for-each select="$object">
       <xsl:if test="not(empty(.))">
        <xsl:variable name="literal" select="map:get(.,'value')" as="xs:string"/>
        <xsl:variable name="datatype" select="if (map:contains(.,'datatype'))
            then map:get(.,'datatype')
            else ()" as="xs:string?"/>
        <xsl:variable name="language" select="if (map:contains(.,'lang'))
            then map:get(.,'lang')
            else ()" as="xs:string?"/>
        <xsl:variable name="object" as="xs:string">"{flub:escape-literal($literal)}"{if ($language) then '@' else if ($datatype) then '^^&lt;' else ()}{$datatype}{if ($datatype) then '&gt;' else ''}{$language}</xsl:variable>
        <xsl:value-of select="for $x in $object return flub:nqwrite($subject,$predicate('uri'),$object,$graph)"/>
       </xsl:if> 
     </xsl:for-each>
    </xsl:function>
        
    <xsl:variable name="DQUOTE"><xsl:text>"</xsl:text></xsl:variable>
    <xsl:variable name="QUOTE"><xsl:text>'</xsl:text></xsl:variable>
    <!-- \b and \f causes unparsed-doc to fail. Are illegal characters in xml-->
    <xsl:variable name="escape-sets" as="map(xs:integer,xs:string+)">
        <xsl:map>
            <xsl:map-entry key="1" select="('\\','(\\)')"/>
            <xsl:map-entry key="2" select="('\t','(\t)')"/>             
            <xsl:map-entry key="3" select="('\n','(&#x000A;)')"/>
            <xsl:map-entry key="4" select="('\r','(&#x000D;)')"/>         
            <xsl:map-entry key="5" select="(concat('\',$DQUOTE),'(&#x0022;)')"/>
            <!--<xsl:map-entry key="6" select="concat('\',$QUOTE),concat('(',$QUOTE,')')"/>-->                     
            
        </xsl:map>        
    </xsl:variable>        
    <xsl:variable name="escape-sets-size" select="map:size($escape-sets)" as="xs:integer"/> 
    <xsl:variable name="escape-regex" select="if ($escape-sets-size &gt; 1)
        then (string-join((for $x in 1 to $escape-sets-size 
        return map:get($escape-sets,$x)[2]),'|')) 
        else ''" as="xs:string"/> 
    
    <xsl:function name="flub:xsd-literal" as="map(xs:string,xs:string)?">
        <xsl:param name="literal" as="xs:string?"/>
        <xsl:param name="xsdname" as="xs:string?"/>
        
        <xsl:if test="$literal">
            <xsl:map>
        <xsl:map-entry key="'value'" select="$literal"/>
        <xsl:map-entry key="'datatype'" select="concat($xsd,$xsdname)"/>
        </xsl:map>
        </xsl:if>
    </xsl:function>
    
    <xsl:function name="flub:escape-literal" as="xs:string">
        <xsl:param name="literal" as="xs:string"/>
        <xsl:choose>
            <xsl:when test="not(matches($literal,$escape-regex))">
                <xsl:value-of select="$literal"/>
            </xsl:when>
            <xsl:otherwise>
            <xsl:variable name="results" as="xs:string+">    <xsl:analyze-string select="$literal" regex="{$escape-regex}">
                    <xsl:matching-substring>
                        <xsl:iterate select="1 to $escape-sets-size">
                            <xsl:if test="regex-group(.)">
                                <xsl:value-of select="map:get($escape-sets,.)[1]"/>
                                <xsl:break/>
                            </xsl:if>
                        </xsl:iterate>               
                    </xsl:matching-substring>
                    <xsl:non-matching-substring>
                        <xsl:value-of select="."/>
                    </xsl:non-matching-substring>
                </xsl:analyze-string>
            </xsl:variable>
                <xsl:value-of select="string-join($results,'')"/>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:function>   
     
    <xsl:function name="flub:nqwrite" visibility="private" as="xs:string">
        <xsl:param name="subject" as="xs:string"/>
        <xsl:param name="predicate" as="xs:string"/>
        <xsl:param name="object" as="xs:string"/>
        <xsl:param name="graph" as="xs:string?"/>
<xsl:text>&lt;{$subject}&gt; &lt;{$predicate}&gt; {$object} {if (empty($graph)) then '' else concat('&lt;',$graph,'&gt;')}.
</xsl:text>
</xsl:function>
</xsl:stylesheet>
