<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    exclude-result-prefixes="xs"
    version="2.0">
    <!-- subset av https://en.wikipedia.org/wiki/ISO_8601 som brukes av EAD; der det kun beskrives de datofunksjoner som vi bruker i EAD.-->
    
    <xsl:template match="/">
        <xsl:value-of select="flub:getTimeSpan('1905-01-01/1904')"/>
    </xsl:template>
    <!-- regex-->
    <xsl:variable name="day-regex">(0[1-9]|[1-2][0-9]|3[0-1])</xsl:variable>
    <xsl:variable name="month-regex"><xsl:text>([0][1-9]|1[0-2])</xsl:text></xsl:variable>
    <xsl:variable name="year-regex">[0-9]{4}</xsl:variable>
    <xsl:variable name="yearMonth" select="concat('^',$year-regex,'-',$month-regex,'$')"/>
    <xsl:variable name="date-regex" select="concat('^',$year-regex,'-',$month-regex,'-',$day-regex,'$')"/>
    
    <xsl:variable name="validDate">
        (<xsl:value-of select="$year-regex"/>|<xsl:value-of select="$year-regex"/>-<xsl:value-of select="$month-regex"/>|<xsl:value-of select="$year-regex"/>-<xsl:value-of select="$month-regex"/>-<xsl:value-of select="$day-regex"/>)
    </xsl:variable>

    <xsl:function name="flub:tokenizeTimeSpan">
        <xsl:param name="normal-date"/>
        <xsl:variable name="dates" select="tokenize($normal-date,'/')" as="xs:string+"/>
        <xsl:variable name="date-regex" select="$date-regex"/>
        <!--<xsl:variable name="validDate-regex" select="concat('^',$validDate,'$')"/>-->
        <xsl:if test="$dates[matches(.,$date-regex) and not(. castable as xs:date)](:: or $dates[not(matches(.,concat('^',$validDate-regex,'$')))]::) or count($dates) &gt; 2">
            <xsl:message terminate="yes">flub:isTimeInterval terminate: Invalid date <xsl:value-of select="$normal-date"/>
            </xsl:message>
        </xsl:if>     
        <xsl:sequence select="$dates"/>
    </xsl:function>
    
 <!--   <xsl:function name="flub:isTimeInterval" as="xs:boolean">
        <xsl:param name="normal-date" as="xs:string"/>       
        <xsl:if test="flub:tokenizeTimeSpan($normal-date)"/>           
        
        <xsl:value-of select="if (matches($normal-date,concat('^',$validDate,'/',$validDate,'$'))) then true() else false()"/>
    </xsl:function> -->   
    
   <!-- <xsl:function name="flub:getDatatypeFromDate">
        <xsl:param name="date"/>
        <xsl:variable name="datatype">
        <xsl:choose>
            <xsl:when test="matches($date,$date-regex) and $date castable as xs:date">
                <xsl:value-of select="'http://www.w3.org/2001/XMLSchema#date'"/>
            </xsl:when>
            <xsl:when test="matches($date,concat('^',$year-regex,'$')) and $date castable as xs:gYear">
                <xsl:value-of select="'http://www.w3.org/2001/XMLSchema#gYear'"/>
            </xsl:when>
            <xsl:when test="matches($date,$yearMonth) and $date castable as xs:gYearMonth">
                <xsl:value-of select="'http://www.w3.org/2001/XMLSchema#gYearMonth'"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message terminate="yes">flub:getDatatypeFromDate terminate: datatype not found for date: <xsl:value-of select="$date"/></xsl:message>
            </xsl:otherwise>
        </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="$datatype"/>
    </xsl:function>-->
    
    <!-- Funksjon som tar inn en @normal og returnerer timespan basert på denne-->
    <xsl:function name="flub:getTimeSpan" as="xs:date+">
        <xsl:param name="normal-date" as="xs:string"/>
        <xsl:variable name="tokenize" select="flub:tokenizeTimeSpan($normal-date)" />
       
        <xsl:variable name="results" as="xs:date+">
        <xsl:choose>
            <xsl:when test="count($tokenize)=2">
                <xsl:sequence select="flub:getTimeSpanForDate($tokenize[1])[1],flub:getTimeSpanForDate($tokenize[2])[2]"></xsl:sequence>
            </xsl:when>
            <xsl:otherwise><xsl:sequence select="flub:getTimeSpanForDate($normal-date)"></xsl:sequence></xsl:otherwise>
        </xsl:choose>  
        </xsl:variable>
                                   
        <xsl:if test="xs:date($results[2]) &lt; xs:date($results[1])">
            <xsl:message terminate="yes">flub:getTimeSpan: Terminate: lagd etter <xsl:value-of select="$results[1]"/> er nyere enn lagd før <xsl:value-of select="$results[2]"/></xsl:message>
        </xsl:if>
        <xsl:sequence select="$results"/>
    </xsl:function>
    
    <xsl:function name="flub:getTimeSpanForDate" as="xs:date+">
        <xsl:param name="date"/>
        <xsl:choose>
            <xsl:when test="$date castable as xs:date">
                <xsl:sequence select="xs:date($date),xs:date($date)"/>
            </xsl:when>
            <xsl:when test="$date castable as xs:gYearMonth">
                <xsl:sequence select="flub:getTimeSpanFromGYearMonth( xs:gYearMonth($date))"></xsl:sequence>
            </xsl:when>
            <xsl:when test="$date castable as xs:gYear">
                <xsl:sequence select="flub:getTimeSpanFromGYear(xs:gYear($date))"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message terminate="yes">
                    flub:getTimeSpanForDate terminate: $date: <xsl:value-of select="$date"/>
                </xsl:message>
            </xsl:otherwise>
        </xsl:choose>            
    </xsl:function>
    
    <!-- first item, made after, second item, made before-->
    <xsl:function name="flub:getTimeSpanFromGYear" as="xs:date+">
       <xsl:param name="gYear" as="xs:gYear"/>
       <xsl:variable name="madeAfter" select="concat($gYear,'-01-01') cast as xs:date" as="xs:date"/>
        <xsl:variable name="madeBefore" select="$madeAfter+ xs:yearMonthDuration('P1Y') - xs:dayTimeDuration('P1D')"/>
      <xsl:sequence select="xs:date($madeAfter),xs:date($madeBefore)"/>        
    </xsl:function>
    
    <xsl:function name="flub:getTimeSpanFromGYearMonth" as="xs:date+">
        <xsl:param name="gYearMonth" as="xs:gYearMonth"/>
        <xsl:variable name="madeAfter" select="concat($gYearMonth,'-01') cast as xs:date" as="xs:date"/>
        <xsl:variable name="madeBefore" select="$madeAfter+ xs:yearMonthDuration('P1M') - xs:dayTimeDuration('P1D')"/>
        <xsl:sequence select="xs:date($madeAfter),xs:date($madeBefore)"/>        
    </xsl:function>
    
    
</xsl:stylesheet>