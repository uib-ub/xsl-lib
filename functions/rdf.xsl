<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"    
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:function name="flub:createResource" as="element(rdf:Description)">
        <xsl:param name="type" as="element(rdf:type)+"/>
        <xsl:param name="about" as="xs:string"/>
        <xsl:param name="predicates" as="element(*)*"/>
        <rdf:Description rdf:about="{$about}">
            <xsl:for-each select="$type">
                <xsl:copy>
                    <xsl:copy-of select="."/>
                    <xsl:copy-of select="$predicates"/>
                </xsl:copy>
            </xsl:for-each>
        </rdf:Description>
    </xsl:function>
    
    <xsl:function name="flub:addChildren" as="element(*)">
        <xsl:param name="element" as="element(*)"/>
        <xsl:param name="child-elements" as="element(*)*"/>
        <xsl:for-each select="$element">
            <xsl:variable name="child-nodes" select="child::node()"/>
            <xsl:copy>
                <xsl:copy-of select="@*"/>
                <xsl:copy-of select="child::node()"/>
                <xsl:copy-of select="$child-elements"/>                
            </xsl:copy>
        </xsl:for-each>
        
    </xsl:function>
    
    <xsl:function name="flub:addPredicate" as="element(rdf:Description)">
        <xsl:param name="resource" as="element(rdf:Description)"/>
        <xsl:param name="predicate" as="element(*)*"/>
        <xsl:sequence select="flub:addChildren($resource,$predicate)"/>
    </xsl:function>
    
  
  
    
</xsl:stylesheet>