<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    xmlns:ris="http://data.ub.uib.no/ns/function-library/ris"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:array="http://www.w3.org/2005/xpath-functions/array"
    exclude-result-prefixes="xs math"
    version="3.0">
    <xsl:strip-space elements="*"/>
    <xsl:param name="debug" as="xs:boolean" select="false()"/>
   
    <xsl:variable name="ris-fields" select="map {'TY':'1' ,'A1': '?','A2': '?','A3': '?','A4': '?','AB': '?','AD': '?','AN': '?','AU': '?','AV': '?','BT': '?','C1': '?','C2': '?','C3': '?','C4': '?','C5': '?','C6': '?','C7': '?','C8': '?','CA': '?','CN': '?','CP': '?','CT': '?','CY': '?','DA': '?','DB': '?','DO': '?','DP': '?','ED': '?','EP': '?','ET': '?','ID': '?','IS': '?','J1': '?','J2': '?','JA': '?','JF': '?','JO': '?','KW': '?','L1': '?','L2': '?','L3': '?','L4': '?','LA': '?','LB': '?','LK': '?','M1': '?','M2': '?','M3': '?','N1': '?','N2': '?','NV': '?','OP': '?','PB': '?','PP': '?','PY': '?','RI': '?','RN': '?','RP': '?','SE': '?','SN': '?','SP': '?','ST': '?','T1': '?','T2': '?','T3': '?','TA': '?','TI': '?','TT': '?','U1': '?','U2': '?','U3': '?','U4': '?','U5': '?','UR': '?','VL': '?','VO': '?','Y1': '?','Y2': '?','ER' : '1' }"/> 
    <xsl:variable name="newline" as="xs:string"><xsl:text>
</xsl:text></xsl:variable>    
    
    <xsl:function name="ris:serialize" as="xs:string?" visibility="public">
        <xsl:param name="ris" as="map(xs:string,array(xs:string?))*"/>
        <xsl:param name="options" as="map(xs:string,xs:string)"/>
        <xsl:message select="'ris:serialize(ris,options) not implemented'" terminate="yes"/>
    </xsl:function>
    
     <xsl:function name="ris:serialize" as="xs:string?" visibility="public">
         <xsl:param name="ris" as="map(xs:string,array(xs:string?))*"/>        
         <xsl:variable name="results" as="xs:string*">
          
          <xsl:variable name="unique-keys" select="distinct-values(for $x in $ris return map:keys($x))"/>
          
         <xsl:for-each select="$ris">
             <xsl:value-of select="$newline ||$newline || $newline || ris:pair('TY',array:get(map:get(.,'TY'),1)) || $newline"/>
             <xsl:sequence select="string-join(for $x in map:keys(.)[not(.=('TY','ER'))]
                 return string-join(
                 for $y in 1 to array:size(map:get(.,$x))
                 return ris:pair($x,ris:strip-newline(array:get(map:get(.,$x),$y)) 
                 )                
                 ,$newline),$newline)"/>
             <xsl:value-of select="$newline"/>
             <xsl:variable name="illegal-fields" select="$unique-keys[not(.=map:keys($ris-fields))]"/>
             <xsl:if test="exists($illegal-fields)">
                <xsl:message select="'illegal ris field, key: ' || string-join($illegal-fields,';')" terminate="yes"/> 
             </xsl:if>
             <xsl:sequence select="ris:pair('ER','')"/>
         </xsl:for-each>
         </xsl:variable>
         <xsl:value-of select="string-join($results,'')"/>
     </xsl:function>
    
     <xsl:function name="ris:append" as="map(xs:string,array(xs:string?))?">
         <xsl:param name="ris" as="map(xs:string,array(xs:string?))"/>
         <xsl:param name="key" as="xs:string"/>
         <xsl:param name="values" as="array(xs:string?)"/>
      <xsl:if test="$debug"> 
         <xsl:message select="$key,$values,map:keys($ris)"/>
      </xsl:if>
         <xsl:variable name="current-values" select="if (map:contains($ris,$key)) 
             then map:get($ris,$key)
             else []" as="array(xs:string?)?"/>
         
         
         <xsl:variable name="new-array" select="array:append($current-values,for $x in array:flatten($values) return string(ris:strip-newline($x)) )" as="array(xs:string)"/>
         <xsl:variable name="test" select="if (not(empty($new-array)))
             then map:put($ris,$key,$new-array) else $ris" as="map(xs:string,array(xs:string?))?"/>
         
         
         <xsl:sequence select="$test"/>
     </xsl:function>
    
    <xsl:function name="ris:strip-newline" visibility="private">
        <xsl:param name="value" as="xs:string"/>
        <xsl:value-of select="replace($value,'[\n\r]','')"/>
    </xsl:function>
    
    <xsl:function name="ris:pair" visibility="private" as="xs:string">
        <xsl:param name="key" as="xs:string"/>
        <xsl:param name="value" as="xs:string"/>
      
        <xsl:if test="$debug">
            <xsl:message select="'ris pair '||$key,$value"/>
        </xsl:if>
        <xsl:value-of select="$key || '  - ' || $value"/>
        
    </xsl:function>
    
    <!-- returns one map per ris post-->
    <xsl:function name="ris:parse" as="map(xs:string,array(xs:string?))*">
        <xsl:param name="path" as="xs:string"/>
        <xsl:choose>
            <xsl:when test="unparsed-text-available($path)">
                <xsl:iterate select="unparsed-text-lines($path)[contains(., '-')]">
                    <xsl:param name="ris" as="map(xs:string,array(xs:string))?" select="map{}"/>
                    <xsl:param name="ris-maps" as="map(xs:string,array(xs:string?))*" select="()"/>
                    <xsl:on-completion>
                        <xsl:if test="map:size($ris) != 0">
                            <xsl:message select="'unclosed RIS'" terminate="yes"/>
                        </xsl:if>
                        <xsl:sequence select="$ris-maps"/>
                    </xsl:on-completion>                                        
                    <xsl:variable name="key" select="substring(., 1, 2)" as="xs:string?"/>                    
                    <xsl:variable name="value" select="replace(substring-after(., '-'),'[\n\r]','')" as="xs:string?"/>
                    
                    <xsl:if test="not(map:contains($ris-fields, $key)) and not(matches(., '^\s$'))">
                        <xsl:message select="'unknown ris key' || ." terminate="yes"/>
                    </xsl:if>
                    
                    <xsl:if test="$debug">
                        <xsl:message select="$key || ' ' || $value"/>
                    </xsl:if>
                    
                    <!-- ER is added on serialization-->
                    <xsl:choose>
                        <xsl:when test="starts-with(., 'ER  - ')">
                            <xsl:next-iteration>
                                <xsl:with-param name="ris" select="map{}"/>
                                <xsl:with-param name="ris-maps" select="$ris-maps, $ris"/>
                            </xsl:next-iteration>
                        </xsl:when>                        
                        <xsl:when test="starts-with(.,'TY  - ') and map:size($ris)!=0">
                            <xsl:message select="'unclosed RIS when beginning new TY'" terminate="yes"/>
                        </xsl:when>
                        <xsl:when test="matches(., '^\s*$')">
                            <xsl:next-iteration/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:next-iteration>
                                <xsl:with-param name="ris"
                                    select="
                                        if (map:contains($ris, $key))
                                        then
                                            map:put($ris, $key,
                                            array:append(map:get($ris, $key), $value))
                                        else
                                            map:put($ris, $key, [$value])"
                                />
                            </xsl:next-iteration>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:iterate>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message select="$path || ' not found ' || resolve-uri($path)"/>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:function>
</xsl:stylesheet>