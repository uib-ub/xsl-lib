<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    exclude-result-prefixes="xs"
    version="3.0">
    <!-- http://data.bibsys.no/data/authority?-->
    <!-- http://def.bibsys.no/xmlns/radatana/1.0#catalogueName>-->
    
    <xsl:include href="../sparql-lookup.xsl"/>
   
    <xsl:variable name="bibsys-authority-endpoint" select="'http://data.bibsys.no/data/authority?query='"/>
    <xsl:function name="flub:personFromCatalogNameBibsys">
        <xsl:param name="catalog-name"/>
        
<xsl:variable name="query" expand-text="1">
    <xsl:text>PREFIX foaf: &lt;http://xmlns.com/foaf/0.1/> 
PREFIX rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#> 
PREFIX radatana: &lt;http://def.bibsys.no/xmlns/radatana/1.0#>
PREFIX whois: &lt;http://www.kanzaki.com/ns/whois#>
PREFIX skos: &lt;http://www.w3.org/2004/02/skos/core#>
PREFIX : &lt;http://dbpedia.org/resource/>
PREFIX owl: &lt;http://www.w3.org/2002/07/owl#>
PREFIX dc: &lt;http://purl.org/dc/terms/>
PREFIX bibo: &lt;http://purl.org/ontology/bibo/>

        describe ?s where {{ ?s &lt;http://def.bibsys.no/xmlns/radatana/1.0#catalogueName> "{$catalog-name}"}}</xsl:text>
</xsl:variable>   
   <xsl:sequence select="flub:sparqlQuery($query,$bibsys-authority-endpoint)"/>
</xsl:function>


    
    
    
</xsl:stylesheet>