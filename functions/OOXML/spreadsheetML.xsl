<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:flub="http://data.ub.uib.no/ns/function-library/"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:arch="http://expath.org/ns/archive"
    xmlns:saxon="http://saxon.sf.net/" xmlns:file="http://expath.org/ns/file"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:ssml="http://schemas.openxmlformats.org/spreadsheetml/2006/main"
    version="3.0">
    <xsl:param name="debug" as="xs:boolean" select="false()" static="1" />    
    <xsl:key name="lookup_by_string_xslx_counter" match="ssml:t">
        <xsl:sequence select="count(ancestor::ssml:si/preceding-sibling::ssml:si)"/>
    </xsl:key><!-- this xmlns -->
    
    <xsl:key name="xf-lookups" match="ssml:xf">
        <xsl:sequence select="count(preceding-sibling::ssml:xf)"></xsl:sequence>
    </xsl:key>  
    
        <xsl:function name="flub:getDateTimeFromDays" as="xs:dateTime">
        <!-- excel input is decimal of days sinc-->        
        <xsl:param name="daysSinceJanuary" as="xs:decimal"/>
        <!--86400 seconds in a day-->
        
        <xsl:variable name="posNum" as="xs:boolean" select="if ($daysSinceJanuary &lt; 0) then false() else true()  "/>
        <!-- replace content with from decimal point, and remove minus-->
        <xsl:variable name="days0" select="replace(
            replace(string($daysSinceJanuary),'\..+$','')
            ,'^-','')"/>
            <xsl:variable name="feb-29-1900" select="if($daysSinceJanuary &gt; 60) then 2.0 else 1.0"/>
        <xsl:variable name="days" select="if ($days0 != '0') 
            then concat(string(xs:integer($days0)-$feb-29-1900),'D') 
            else ''"/>
            
            <xsl:message select="'days',$days" use-when="$debug"/>
        <xsl:variable name="rest" select="xs:decimal(concat('0.',(substring-after(string($daysSinceJanuary),'.')[string(.)][1]),'0'))" as="xs:decimal"/>
            <xsl:message select="'rest ',$rest" use-when="$debug"/>
            <xsl:variable name="extra-decimal-length" select="(string-length(string($rest))-2)-8"/>
        <xsl:variable name="test" select="if ($extra-decimal-length > 0) 
            then xs:decimal(math:exp10($extra-decimal-length)) 
            else 1.0" as="xs:decimal"/>
        <!-- max precision for saxon duration seems to be 9 rounded to 6?-->
            <xsl:message select="'test',$test"></xsl:message>
        <xsl:variable name="seconds" select="if ($rest!= 0.0)
            then concat('T',format-number(xs:decimal(($rest*$test)*(86400.0 div $test)),'0.000000000'),'S') 
            else ''"/>       
            <xsl:message select="'seconds: ',$seconds" use-when="$debug"></xsl:message>
        <xsl:variable name="duration" select="xs:dayTimeDuration(concat('P',$days,$seconds))" as="xs:dayTimeDuration"/>
            <xsl:message select="'duration',$duration" use-when="$debug"/>
            <xsl:variable name="starting-point" select="xs:dateTime('1900-01-01T00:00:00')" as="xs:dateTime"/>
        <xsl:sequence select="if ($posNum) 
            
            then $starting-point + $duration 
            else $starting-point - $duration"/>
    </xsl:function>
    
    <xsl:variable name="formatTable">
        <table>
            <row format-code="0" picture="0" type="string"/>
            <row format-code="1" picture="0" type="number"/>
            <row format-code="2" picture="0.00" type="number"/>
            <row format-code="3" picture="#,##0" type="number"/>
            <row format-code="4" picture="#,##0.00" type="number"/>
            <row format-code="9" picture="0%" type="number"/>
            <row format-code="10" picture="0.00%" type="number"/>
            <row format-code="11" picture="0.00e+00" type="number"/>
            <row format-code="12" picture="# ?/?" type="number"/>
            <row format-code="13" picture="# ??/??" type="number"/>
            <row format-code="14" picture="[M01]-[D01]-[Y0001]" type="dateTime"/>
            <row format-code="15" picture="[D1]-[MNn]-[Y0001]" type="dateTime"/>
            <row format-code="16" picture="[D1]-[MNn]" type="dateTime"/>
            <row format-code="17" picture="[MNn]-[Y0001]" type="dateTime"/>
            <row format-code="18" picture="[h01]:[m01] [P]" type="dateTime"/>
            <row format-code="19" picture="[h01]:[m01]:[s01] [P]" type="dateTime"/>
            <row format-code="20" picture="[H01]:[m01]" type="dateTime"/>
            <row format-code="21" picture="[H01]:[m01]:[s01]" type="dateTime"/>
            <row format-code="22" picture="m/d/yy h:mm" type="dateTime"/>
            <row format-code="37" picture="#,##0 ;(#,##0)" type="number"/>
            <row format-code="38" picture="#,##0 ;-(#,##0)" type="number"/>
            <row format-code="39" picture="#,##0.00;(#,##0.00)" type="number"/>
            <row format-code="40" picture="#,##0.00;-(#,##0.00)" type="number"/>
            <row format-code="45" picture="[M01]:[s01]" type="dateTime"/>
            <!-- <row format-code="46" picture="[h]:mm:ss" type="dateTime"/>-->
            <row format-code="47" picture="[M01][s01].[f]" type="dateTime"/>
            <row format-code="48" picture="##0.0E+0" type="number"/>
            <row format-code="49" picture="0.00" type="number"/>
        </table>
    </xsl:variable>
    
    <xsl:function name="flub:numberFormat" as="xs:string">
        <xsl:param name="number"/>
        <xsl:param name="formatCode" as="xs:integer"/>
        
        <xsl:variable name="thisFormat" select="$formatTable/table/row[@format-code=$formatCode]"/>
        <xsl:choose>
            <xsl:when test="$thisFormat/@type='string'">
                <xsl:value-of select="$number"/>
            </xsl:when>
            <xsl:when test="$thisFormat/@type='number'">
                <xsl:value-of select="format-number($number,$thisFormat/@picture)"/>                                
            </xsl:when>
            <xsl:when test="$thisFormat/@type='dateTime'">
                <xsl:value-of select="format-dateTime(flub:getDateTimeFromDays($number),$thisFormat/@picture)"/>
            </xsl:when>
            <xsl:when test="$formatCode=0">
                <xsl:value-of select="$number"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message terminate="yes">
                    number: <xsl:value-of select="$number"/> format-code: <xsl:value-of select="$formatCode"/>
                    undefined Format in excel file. Add to table to handle.
                </xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    <!-- 
        The chosen answer is spot-on, but note that Excel defines some number format (numFmt) codes differently from the OpenXML spec. Per the Open XML SDK 2.5 Productivity Tool's documentation (on the "Implementer Notes" tab for the NumberingFormat class):
        
        Excel defines built-in format ID
14: "m/d/yyyy"
22: "m/d/yyyy h:mm"
37: "#,##0_);(#,##0)"
38: "#,##0_);[Red]"
39: "#,##0.00_);(#,##0.00)"
40: "#,##0.00_);[Red]"
47: "mm:ss.0"
55: "yyyy/mm/dd"
        
        0 General
1 0
2 0.00
3 #,##0
4 #,##0.00
9 0%
10 0.00%18. SpreadsheetML Reference Material
ID
formatCode
11 0.00E+00
12 # ?/?
13 # ??/??
14 mm-dd-yy
15 d-mmm-yy
16 d-mmm
17 mmm-yy
18 h:mm AM/PM
19 h:mm:ss AM/PM
20 h:mm
21 h:mm:ss
22 m/d/yy h:mm
37 #,##0 ;(#,##0)
38 #,##0 ;[Red](#,##0)
39 #,##0.00;(#,##0.00)
40 #,##0.00;[Red](#,##0.00)
45 mm:ss
46 [h]:mm:ss
47 mmss.0
48 ##0.0E+0
        -->
    
</xsl:stylesheet>