<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    exclude-result-prefixes="xs math"
    version="3.0" expand-text="1">
    
    <xsl:variable name="xsd" select="'http://www.w3.org/2001/XMLSchema#'"/>
    
    <!-- map of literal key "value" - "literal
        and "datatype" "uri" "lang"        where datatype is optional-->    
    
    <xsl:function name="flub:boolean-literal" as="map(xs:string,xs:string)?">
        <xsl:param name="value" as="xs:boolean?"/>
        <xsl:if test="exists($value)">
            <xsl:map>        
                <xsl:map-entry key="'value'" select="string($value)"/> 
                <xsl:map-entry select="concat($xsd,'boolean')" key="'datatype'"/>          
            </xsl:map>
        </xsl:if>
    </xsl:function>
    
    <xsl:function name="flub:string" as="map(xs:string,xs:string)?">
        <xsl:param name="value" as="xs:string?"/>
        <xsl:sequence select="flub:string($value,())"/>
    </xsl:function>
    
    <xsl:function name="flub:string" as="map(xs:string,xs:string)?">
        <xsl:param name="value" as="xs:string?"/>
        <xsl:param name="lang" as="xs:string?"/>
        <xsl:if test="$value">
            <xsl:map>
                <xsl:map-entry key="'value'" select="$value"/>
                <xsl:if test="$lang">
                    <xsl:map-entry key="'lang'" select="$lang"/>
                </xsl:if>
            </xsl:map>
        </xsl:if>
    </xsl:function>
    
    <xsl:function name="flub:xsd-literal" as="map(xs:string,xs:string)?">
        <xsl:param name="literal" as="xs:string?"/>
        <xsl:param name="xsdname" as="xs:string?"/>
        
        <xsl:if test="$literal">
            <xsl:map>
                <xsl:map-entry key="'value'" select="$literal"/>
                <xsl:map-entry key="'datatype'" select="concat($xsd,$xsdname)"/>
            </xsl:map>
        </xsl:if>
    </xsl:function>
    
    <xsl:function name="flub:resource" as="map(xs:string,xs:string)*">
        <xsl:param name="resource-uri" as="item()*"/>
        
        <xsl:sequence select="for $x in $resource-uri 
            return map {'uri' : $x }"/>
    </xsl:function>
</xsl:stylesheet>