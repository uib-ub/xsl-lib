<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:template match="*" mode="html">
        <xsl:apply-templates mode="html"/>
    </xsl:template>

    <xsl:template match="*:p" mode="html">
        <xsl:text>&lt;p&gt;</xsl:text>
        <xsl:variable name="content">
            <xsl:next-match/>
        </xsl:variable>
        <xsl:value-of select="normalize-space($content)"/>
        <xsl:text>&lt;/p&gt;</xsl:text>

    </xsl:template>

</xsl:stylesheet>